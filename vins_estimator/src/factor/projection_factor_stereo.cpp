#include "projection_factor_stereo.h"

Eigen::Matrix2d ProjectionFactorStereo::sqrt_info;
double ProjectionFactorStereo::sum_t;

ProjectionFactorStereo::ProjectionFactorStereo(const Eigen::Vector3d &_pts_i, const Eigen::Vector3d &_pts_j) : 
                                                               pts_i(_pts_i), pts_j(_pts_j)
{

};

bool ProjectionFactorStereo::Evaluate(double const *const *parameters, double *residuals, double **jacobians) const
{
    TicToc tic_toc;
    // Eigen::Vector3d Pi(parameters[0][0], parameters[0][1], parameters[0][2]);
    // Eigen::Quaterniond Qi(parameters[0][6], parameters[0][3], parameters[0][4], parameters[0][5]);

    // Eigen::Vector3d Pj(parameters[1][0], parameters[1][1], parameters[1][2]);
    // Eigen::Quaterniond Qj(parameters[1][6], parameters[1][3], parameters[1][4], parameters[1][5]);

    // Eigen::Vector3d tic(parameters[2][0], parameters[2][1], parameters[2][2]);
    // Eigen::Quaterniond qic(parameters[2][6], parameters[2][3], parameters[2][4], parameters[2][5]);

    // Eigen::Vector3d tic1(parameters[3][0], parameters[3][1], parameters[3][2]);
    // Eigen::Quaterniond qic1(parameters[3][6], parameters[3][3], parameters[3][4], parameters[3][5]);
    Eigen::Map<const Eigen::Vector3d> Pi(parameters[0]);
    Eigen::Map<const Eigen::Quaterniond> Qi(parameters[0] + 3);

    Eigen::Map<const Eigen::Vector3d> Pj(parameters[1]);
    Eigen::Map<const Eigen::Quaterniond> Qj(parameters[1] + 3);

    Eigen::Map<const Eigen::Vector3d> tic(parameters[2]);
    Eigen::Map<const Eigen::Quaterniond> qic(parameters[2] + 3);

    Eigen::Map<const Eigen::Vector3d> tic1(parameters[3]);
    Eigen::Map<const Eigen::Quaterniond> qic1(parameters[3] + 3);

    double inv_dep_i = parameters[4][0];

    // std::cout << "Qi w = " << Qi.w() << std::endl;
    // std::cout << "Qi x = " << Qi.x() << std::endl;
    // std::cout << "Qi y = " << Qi.y() << std::endl;
    // std::cout << "Qi z = " << Qi.z() << std::endl;
 
    Eigen::Vector3d pts_camera_i = pts_i / inv_dep_i;
    Eigen::Vector3d pts_imu_i = qic * pts_camera_i + tic;
    Eigen::Vector3d pts_w = Qi * pts_imu_i + Pi;
    // Eigen::Vector3d pts_imu_j = Qj.inverse() * (pts_w - Pj);
    Eigen::Vector3d pts_imu_j = Qj.conjugate() * (pts_w - Pj);
    // Eigen::Vector3d pts_camera_j = qic1.inverse() * (pts_imu_j - tic1);
    Eigen::Vector3d pts_camera_j = qic1.conjugate() * (pts_imu_j - tic1);
    Eigen::Map<Eigen::Vector2d> residual(residuals);

    double dep_j = pts_camera_j.z();
    residual = (pts_camera_j / dep_j).head<2>() - pts_j.head<2>();

    residual = sqrt_info * residual;

    // std::cout << "residual[0] = " << residual[0] << std::endl;
    // std::cout << "residual[1] = " << residual[1] << std::endl;

    if (jacobians)
    {
        Eigen::Matrix3d Ri = Qi.toRotationMatrix();
        Eigen::Matrix3d Rj = Qj.toRotationMatrix();
        Eigen::Matrix3d ric = qic.toRotationMatrix();
        Eigen::Matrix3d ric1 = qic1.toRotationMatrix();
        Eigen::Matrix<double, 2, 3> J1(2, 3);

        Eigen::Matrix3d ric_T_Rj_T = ric1.transpose() * Rj.transpose();
        Eigen::Matrix3d ric_T_Rj_T_Ri = ric_T_Rj_T * Ri;
        Eigen::Matrix3d ric_T_Rj_T_Ri_ric = ric_T_Rj_T_Ri * ric;

        J1 << 1. / dep_j, 0, -pts_camera_j(0) / (dep_j * dep_j),
            0, 1. / dep_j, -pts_camera_j(1) / (dep_j * dep_j);

        J1 = sqrt_info * J1;

        if (jacobians[0])
        {
            Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacobian_pose_i(jacobians[0]);

            Eigen::Matrix<double, 3, 6> jaco_i;
            jaco_i.leftCols<3>() = ric_T_Rj_T;
            jaco_i.rightCols<3>() = ric_T_Rj_T_Ri * -Utility::skewSymmetric(pts_imu_i);

            jacobian_pose_i.leftCols<6>() = J1 * jaco_i;
            jacobian_pose_i.rightCols<1>().setZero();
        }

        if (jacobians[1])
        {
            Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacobian_pose_j(jacobians[1]);

            Eigen::Matrix<double, 3, 6> jaco_j;
            jaco_j.leftCols<3>() = ric1.transpose() * -Rj.transpose();
            jaco_j.rightCols<3>() = ric1.transpose() * Utility::skewSymmetric(pts_imu_j);

            jacobian_pose_j.leftCols<6>() = J1 * jaco_j;
            jacobian_pose_j.rightCols<1>().setZero();
        }
        if (jacobians[2])
        {
            Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacobian_ex_pose(jacobians[2]);
            Eigen::Matrix<double, 3, 6> jaco_ex;
            jaco_ex.leftCols<3>() = ric_T_Rj_T_Ri; 
            jaco_ex.rightCols<3>() = ric_T_Rj_T_Ri_ric * -Utility::skewSymmetric(pts_camera_i);
            jacobian_ex_pose.leftCols<6>() = J1 * jaco_ex;
            jacobian_ex_pose.rightCols<1>().setZero();
        }

        if (jacobians[3])
        {
            Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacobian_ex_pose1(jacobians[3]);
            Eigen::Matrix<double, 3, 6> jaco_ex;
            jaco_ex.leftCols<3>() = - ric1.transpose();
            jaco_ex.rightCols<3>() = Utility::skewSymmetric(pts_camera_j);
            jacobian_ex_pose1.leftCols<6>() = J1 * jaco_ex;
            jacobian_ex_pose1.rightCols<1>().setZero();
        }


        if (jacobians[4])
        {
            Eigen::Map<Eigen::Vector2d> jacobian_feature(jacobians[4]);
            jacobian_feature = J1 * -1.0 / (inv_dep_i * inv_dep_i) * ric_T_Rj_T_Ri_ric * pts_i;
        }
    }
    sum_t += tic_toc.toc();

    return true;
}

void ProjectionFactorStereo::check(double **parameters)
{

}
