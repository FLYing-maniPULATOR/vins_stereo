#include "pose_local_parameterization.h"

bool PoseLocalParameterization::Plus(const double *x, const double *delta, double *x_plus_delta) const
{
    Eigen::Map<const Eigen::Vector3d> _p(x);
    Eigen::Map<const Eigen::Quaterniond> _q(x + 3);

    Eigen::Map<const Eigen::Vector3d> dp(delta);

    Eigen::Quaterniond dq = Utility::deltaQ(Eigen::Map<const Eigen::Vector3d>(delta + 3));

    Eigen::Map<Eigen::Vector3d> p(x_plus_delta);
    Eigen::Map<Eigen::Quaterniond> q(x_plus_delta + 3);

    p = _p + dp;
    q = (_q * dq).normalized();

    return true;
}
bool PoseLocalParameterization::ComputeJacobian(const double *x, double *jacobian) const
{
    Eigen::Map<Eigen::Matrix<double, 7, 6, Eigen::RowMajor>> j(jacobian);
    // Eigen::Map<const Eigen::Quaternion<double>> q(x+3);
    
    // j.setZero();
    // j.block<3,3>(0,0).setIdentity();

    // // x[3] = qx; x[4] = qy; x[5] = qz; x[6] = qw;

    // j(3,3) = -x[3]; j(3,4) = -x[4]; j(3,5)= -x[5];
    // j(4,3) =  x[6]; j(4,4) = -x[5]; j(4,5)=  x[4];
    // j(5,3) =  x[5]; j(5,4) =  x[6]; j(5,5)= -x[3];
    // j(6,3) = -x[4]; j(6,4) =  x[3]; j(6,5)=  x[6];

    // j.block<4,3>(3,3) *= 0.5;

    // std::cout << "J:" << j <<std::endl;

    j.topRows<6>().setIdentity();
    j.bottomRows<1>().setZero();

    return true;
}
