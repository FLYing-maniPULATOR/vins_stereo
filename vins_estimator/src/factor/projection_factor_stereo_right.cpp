#include "projection_factor_stereo_right.h"

Eigen::Matrix2d ProjectionFactorStereoRight::sqrt_info;
double ProjectionFactorStereoRight::sum_t;

// _pts_i: measurement in cr_i
// _pts_j: measurement in cl_j
ProjectionFactorStereoRight::ProjectionFactorStereoRight(const Eigen::Vector3d &_pts_i, const Eigen::Vector3d &_pts_j) : 
                                                               pts_i(_pts_i), pts_j(_pts_j)
{

};

bool ProjectionFactorStereoRight::Evaluate(double const *const *parameters, double *residuals, double **jacobians) const
{
    TicToc tic_toc;
    Eigen::Vector3d Pi(parameters[0][0], parameters[0][1], parameters[0][2]);
    Eigen::Quaterniond Qi(parameters[0][6], parameters[0][3], parameters[0][4], parameters[0][5]);

    Eigen::Vector3d Pj(parameters[1][0], parameters[1][1], parameters[1][2]);
    Eigen::Quaterniond Qj(parameters[1][6], parameters[1][3], parameters[1][4], parameters[1][5]);

    Eigen::Vector3d tic(parameters[2][0], parameters[2][1], parameters[2][2]);
    Eigen::Quaterniond qic(parameters[2][6], parameters[2][3], parameters[2][4], parameters[2][5]);

    Eigen::Vector3d tic1(parameters[3][0], parameters[3][1], parameters[3][2]);
    Eigen::Quaterniond qic1(parameters[3][6], parameters[3][3], parameters[3][4], parameters[3][5]);

    double inv_dep_i = parameters[4][0];
 
    Eigen::Vector3d pts_cr_i = pts_i / inv_dep_i;
    Eigen::Vector3d pts_imu_i = qic1 * pts_cr_i + tic1;
    Eigen::Vector3d pts_w = Qi * pts_imu_i + Pi;
    Eigen::Vector3d pts_imu_j = Qj.inverse() * (pts_w - Pj);
    Eigen::Vector3d pts_cl_j = qic.inverse() * (pts_imu_j - tic);
    Eigen::Map<Eigen::Vector2d> residual(residuals);

    double dep_j = pts_cl_j.z();
    residual = (pts_cl_j / dep_j).head<2>() - pts_j.head<2>();

    residual = sqrt_info * residual;

    if (jacobians)
    {
        Eigen::Matrix3d Ri = Qi.toRotationMatrix();
        Eigen::Matrix3d Rj = Qj.toRotationMatrix();
        Eigen::Matrix3d ric = qic.toRotationMatrix();
        Eigen::Matrix3d ric1 = qic1.toRotationMatrix();
        Eigen::Matrix<double, 2, 3> J1(2, 3);

        J1 << 1. / dep_j, 0, -pts_cl_j(0) / (dep_j * dep_j),
            0, 1. / dep_j, -pts_cl_j(1) / (dep_j * dep_j);

        J1 = sqrt_info * J1;

        if (jacobians[0])
        {
            Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacobian_pose_i(jacobians[0]);

            Eigen::Matrix<double, 3, 6> jaco_i;
            jaco_i.leftCols<3>() = ric.transpose() * Rj.transpose();
            jaco_i.rightCols<3>() = ric.transpose() * Rj.transpose() * Ri * -Utility::skewSymmetric(pts_imu_i);

            jacobian_pose_i.leftCols<6>() = J1 * jaco_i;
            jacobian_pose_i.rightCols<1>().setZero();
        }

        if (jacobians[1])
        {
            Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacobian_pose_j(jacobians[1]);

            Eigen::Matrix<double, 3, 6> jaco_j;
            jaco_j.leftCols<3>() = -ric.transpose() * Rj.transpose();
            jaco_j.rightCols<3>() = ric.transpose() * Utility::skewSymmetric(pts_imu_j);

            jacobian_pose_j.leftCols<6>() = J1 * jaco_j;
            jacobian_pose_j.rightCols<1>().setZero();
        }
        if (jacobians[2])
        {
            Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacobian_ex_pose(jacobians[2]);
            Eigen::Matrix<double, 3, 6> jaco_ex;
            jaco_ex.leftCols<3>() = -ric.transpose(); 
            jaco_ex.rightCols<3>() = Utility::skewSymmetric(pts_cl_j);
            jacobian_ex_pose.leftCols<6>() = J1 * jaco_ex;
            jacobian_ex_pose.rightCols<1>().setZero();
        }
        if (jacobians[3])
        {
            Eigen::Map<Eigen::Matrix<double, 2, 7, Eigen::RowMajor>> jacobian_ex_pose1(jacobians[3]);
            Eigen::Matrix<double, 3, 6> jaco_ex;
            jaco_ex.leftCols<3>() = ric.transpose() * Rj.transpose() * Ri;
            jaco_ex.rightCols<3>() = ric.transpose() * Rj.transpose() * Ri * ric1 * -Utility::skewSymmetric(pts_cr_i);
            jacobian_ex_pose1.leftCols<6>() = J1 * jaco_ex;
            jacobian_ex_pose1.rightCols<1>().setZero();
        }

        if (jacobians[4])
        {
            Eigen::Map<Eigen::Vector2d> jacobian_feature(jacobians[4]);
            jacobian_feature = J1 * ric.transpose() * Rj.transpose() * Ri * ric1 * pts_i * -1.0 / (inv_dep_i * inv_dep_i);
        }
    }
    sum_t += tic_toc.toc();

    return true;
}

void ProjectionFactorStereoRight::check(double **parameters)
{

}
