#include "feature_manager.h"

int FeaturePerId::endFrame()
{
    return start_frame + feature_per_frame.size() - 1;
}

FeatureManager::FeatureManager(Matrix3d _Rs[])
    : Rs(_Rs)
{
    for (int i = 0; i < NUM_OF_CAM; i++)
        ric[i].setIdentity();
}

void FeatureManager::setRic(Matrix3d _ric[])
{
    for (int i = 0; i < NUM_OF_CAM; i++)
    {
        ric[i] = _ric[i];
    }
}

void FeatureManager::clearState()
{
    feature.clear();
}

int FeatureManager::getFeatureCount()
{
    int cnt = 0;
    for (auto &it : feature)
    {

        it.used_num = it.feature_per_frame.size();

        if (it.used_num >= 2 && it.start_frame < WINDOW_SIZE - 2)
        {
            cnt++;
        }
    }
    return cnt;
}

bool FeatureManager::addFeatureCheckParallax(int frame_count, const map<int, vector<pair<int, Eigen::Matrix<double, 12, 1>>>> &image, double td)
{
    ROS_DEBUG("input feature: %d", (int)image.size());
    ROS_DEBUG("num of feature: %d", getFeatureCount());
    double parallax_sum = 0;
    int parallax_num = 0;
    last_track_num = 0; // number of tracked features
    for (auto &id_pts : image)
    {
        // id_pts.second[0].second = xyz_uv_velocity_xyz_uv_right
        FeaturePerFrame f_per_fra(id_pts.second[0].second, td);

        int feature_id = id_pts.first;
        // check if the feature is already exist
        auto it = find_if(feature.begin(), feature.end(), [feature_id](const FeaturePerId &it) {
            return it.feature_id == feature_id;
        });
        // feature not exist, add new feature
        if (it == feature.end())
        {
            // FeaturePerId(id, start_frame)
            feature.push_back(FeaturePerId(feature_id, frame_count));
            // xyz_uv_velocity of the feature
            feature.back().feature_per_frame.push_back(f_per_fra);

            // cout << "point left: " << f_per_fra.point.transpose() << endl;
            // cout << "uv left: " << f_per_fra.uv.transpose() << endl;
            // cout << "point right: " << f_per_fra.point_right.transpose() << endl;
            // cout << "uv right: " << f_per_fra.uv_right.transpose() << endl;
        }
        // feature exist
        else if (it->feature_id == feature_id)
        {
            // add xyz_uv_velocity measurement
            it->feature_per_frame.push_back(f_per_fra);
            last_track_num++;
        }
    }

    // cout << "last_track_num = " << last_track_num << endl;

    // frame_count = 0,1 ==> in ininitialization 
    // or track quality is low, this is a keyframe ==> MARGIN_OLD
    if (frame_count < 2 || last_track_num < 20)
        return true;

    for (auto &it_per_id : feature)
    {
        if (it_per_id.start_frame <= frame_count - 2 &&
            it_per_id.start_frame + int(it_per_id.feature_per_frame.size()) - 1 >= frame_count - 1)
        {
            parallax_sum += compensatedParallax2(it_per_id, frame_count);
            parallax_num++;
        }
    }

    // no feature can be used for compute parallax, observation < 2
    // ==> MARGIN_OLD
    if (parallax_num == 0)
    {
        return true;
    }
    else
    {
        ROS_DEBUG("parallax_sum: %lf, parallax_num: %d", parallax_sum, parallax_num);
        ROS_DEBUG("current parallax: %lf", parallax_sum / parallax_num * FOCAL_LENGTH);
        return parallax_sum / parallax_num >= MIN_PARALLAX;
    }
}

void FeatureManager::debugShow()
{
    ROS_DEBUG("debug show");
    for (auto &it : feature)
    {
        ROS_ASSERT(it.feature_per_frame.size() != 0);
        ROS_ASSERT(it.start_frame >= 0);
        ROS_ASSERT(it.used_num >= 0);

        ROS_DEBUG("%d,%d,%d ", it.feature_id, it.used_num, it.start_frame);
        // int sum = 0;
        // for (auto &j : it.feature_per_frame)
        // {
        //     ROS_DEBUG("%d,", int(j.is_used));
        //     sum += j.is_used;
        //     printf("(%lf,%lf) ", j.point(0), j.point(1));
        // }
        // ROS_ASSERT(it.used_num == sum);
    }
}

vector<pair<Vector3d, Vector3d>> FeatureManager::getCorresponding(int frame_count_l, int frame_count_r)
{
    vector<pair<Vector3d, Vector3d>> corres;
    for (auto &it : feature)
    {
        if (it.start_frame <= frame_count_l && it.endFrame() >= frame_count_r)
        {
            Vector3d a = Vector3d::Zero(), b = Vector3d::Zero();
            int idx_l = frame_count_l - it.start_frame;
            int idx_r = frame_count_r - it.start_frame;

            a = it.feature_per_frame[idx_l].point;

            b = it.feature_per_frame[idx_r].point;

            corres.push_back(make_pair(a, b));
        }
    }
    return corres;
}

void FeatureManager::setDepth(const VectorXd &x)
{
    int feature_index = -1;
    for (auto &it_per_id : feature)
    {
        it_per_id.used_num = it_per_id.feature_per_frame.size();
        if (!(it_per_id.used_num >= 2 && it_per_id.start_frame < WINDOW_SIZE - 2))
            continue;

        it_per_id.estimated_depth = 1.0 / x(++feature_index);
        //ROS_INFO("feature id %d , start_frame %d, depth %f ", it_per_id->feature_id, it_per_id-> start_frame, it_per_id->estimated_depth);
        if (it_per_id.estimated_depth < 0)
        {
            it_per_id.solve_flag = 2;
        }
        else
            it_per_id.solve_flag = 1;
    }
}

void FeatureManager::removeFailures()
{
    for (auto it = feature.begin(), it_next = feature.begin();
         it != feature.end(); it = it_next)
    {
        it_next++;
        if (it->solve_flag == 2)
            feature.erase(it);
    }
}

void FeatureManager::clearDepth(const VectorXd &x)
{
    int feature_index = -1;
    for (auto &it_per_id : feature)
    {
        it_per_id.used_num = it_per_id.feature_per_frame.size();
        if (!(it_per_id.used_num >= 2 && it_per_id.start_frame < WINDOW_SIZE - 2))
            continue;
        it_per_id.estimated_depth = 1.0 / x(++feature_index);
    }
}

VectorXd FeatureManager::getDepthVector()
{
    VectorXd dep_vec(getFeatureCount());
    int feature_index = -1;
    for (auto &it_per_id : feature)
    {
        it_per_id.used_num = it_per_id.feature_per_frame.size();
        if (!(it_per_id.used_num >= 2 && it_per_id.start_frame < WINDOW_SIZE - 2))
            continue;
#if 1
        dep_vec(++feature_index) = 1. / it_per_id.estimated_depth;
#else
        dep_vec(++feature_index) = it_per_id->estimated_depth;
#endif
    }
    return dep_vec;
}

// Pose0: cam0_T_w
// Pose1: cam1_T_w
// point0: point in image(normalized plane)
// point_3d: 3d point in w.r.t. Pose0
void FeatureManager::triangulatePoint(Eigen::Matrix<double, 3, 4> &Pose0, Eigen::Matrix<double, 3, 4> &Pose1,
                                      Vector2d &point0, Vector2d &point1, Vector3d &point_3d)
{
    Matrix4d design_matrix = Matrix4d::Zero();
    design_matrix.row(0) = point0[0] * Pose0.row(2) - Pose0.row(0);
    design_matrix.row(1) = point0[1] * Pose0.row(2) - Pose0.row(1);
    design_matrix.row(2) = point1[0] * Pose1.row(2) - Pose1.row(0);
    design_matrix.row(3) = point1[1] * Pose1.row(2) - Pose1.row(1);
    Vector4d triangulated_point;
    triangulated_point =
        design_matrix.jacobiSvd(Eigen::ComputeFullV).matrixV().rightCols<1>();
    point_3d(0) = triangulated_point(0) / triangulated_point(3);
    point_3d(1) = triangulated_point(1) / triangulated_point(3);
    point_3d(2) = triangulated_point(2) / triangulated_point(3);
}

// return: frame1_T_frame0
bool FeatureManager::solveFrameByPnP(Matrix3d &R_initial, Vector3d &P_initial,
                                     int _frame0, int _frame1, int frame_count)
{
    vector<cv::Point2f> pts_2_vector;
    vector<cv::Point3f> pts_3_vector;

    assert(_frame0 != _frame1);

    int frame0, frame1;

    if (_frame0 > _frame1)
    {
        frame0 = _frame1;
        frame1 = _frame0;
    }
    else
    {
        frame0 = _frame0;
        frame1 = _frame1;
    }

    // frame0 < frame1

    assert((frame0 >= 0) && (frame1 <= frame_count));

    for (auto &it_per_id : feature)
    {
        if (it_per_id.estimated_depth <= 0)
            continue;

        if (it_per_id.start_frame != frame0)
            continue;

        if ((int)it_per_id.feature_per_frame.size() < (frame1 - frame0) + 1)
            continue;

        // 2d point in frame1
        Vector2d point2d = it_per_id.feature_per_frame[frame1 - frame0].point.head(2);
        // 3d point in frame0
        Vector3d point3d = it_per_id.feature_per_frame[0].point * it_per_id.estimated_depth;
        pts_2_vector.emplace_back(point2d(0), point2d(1));
        pts_3_vector.emplace_back(point3d(0), point3d(1), point3d(2));
    }

    if (int(pts_2_vector.size()) < 30)
    {
        printf("solveFrameByPnP: too little features size = %d.\n", (int)pts_2_vector.size());
        if (int(pts_2_vector.size()) < 20)
            return false;
    }
    cv::Mat r, rvec, t, D, tmp_r;
    cv::eigen2cv(R_initial, tmp_r);
    cv::Rodrigues(tmp_r, rvec);
    cv::eigen2cv(P_initial, t);
    cv::Mat K = (cv::Mat_<double>(3, 3) << 1, 0, 0, 0, 1, 0, 0, 0, 1);
    bool pnp_succ;
    pnp_succ = cv::solvePnPRansac(pts_3_vector, pts_2_vector, K, D, 
    rvec, t, false, 100, 5.0/FOCAL_LENGTH, 0.99, cv::noArray(), cv::SOLVEPNP_EPNP);
    if (!pnp_succ)
    {
        cout << "PnP fail." << endl;
        return false;
    }
    cv::Rodrigues(rvec, r);
    //cout << "r " << endl << r << endl;
    MatrixXd R_pnp;
    cv::cv2eigen(r, R_pnp);
    MatrixXd T_pnp;
    cv::cv2eigen(t, T_pnp);
    R_initial = R_pnp; // frame1_R_frame0
    P_initial = T_pnp;
    if (_frame0 > _frame1)
    {
        R_initial = R_initial.transpose();
        P_initial = -R_initial.transpose() * P_initial;
    }
    return true;
}

void FeatureManager::triangulateAllPoints(Vector3d tic[], Matrix3d ric[])
{
    Eigen::Matrix<double, 3, 4> poseL; // c0_l_T_c0 = I
    Eigen::Matrix<double, 3, 4> poseR; // c0_r_T_c0

    poseL.leftCols<3>() = Eigen::Matrix3d::Identity();
    poseL.rightCols<1>() = Eigen::Vector3d::Zero();

    // Eigen::Vector3d left_t_right(0.11981, -0.00024, 0.00023);
    // Eigen::Matrix3d left_R_right;
    // left_R_right << 0.99994, 0.00893, -0.00275,
    //     -0.00895, 0.99995, -0.00221,
    //     0.00271, 0.00223, 0.99999;
    Eigen::Vector3d left_t_right = ric[0].transpose() * (tic[1] - tic[0]);
    Eigen::Matrix3d left_R_right = ric[0].transpose() * ric[1];

    Eigen::Matrix3d right_R_left = left_R_right.transpose();
    Eigen::Vector3d right_t_left = -left_R_right.transpose() * left_t_right;
    poseR.leftCols<3>() = right_R_left * poseL.leftCols<3>();
    poseR.rightCols<1>() = right_R_left * poseL.rightCols<1>() + right_t_left;
    Eigen::Vector2d pointL;
    Eigen::Vector2d pointR;
    Eigen::Vector3d point_3d;

    int points_sum = 0;

    for (auto &it_per_id : feature)
    {
        // if (it_per_id.estimated_depth > 0)
            // continue;
        pointL = it_per_id.feature_per_frame[0].point.head(2);
        pointR = it_per_id.feature_per_frame[0].point_right.head(2);
        triangulatePoint(poseL, poseR, pointL, pointR, point_3d);
        double depth = point_3d(2);
        if (depth > 0.1)
        {
            it_per_id.estimated_depth = depth;
            // cout << "point_3d = " << point_3d.transpose() << endl;
            // cout <<"id: "<< it_per_id.feature_id << "  depth:" << depth << endl;
            points_sum++;
        }
    }
    // cout << "[ triangulateAllPoints] triangulated " << points_sum <<" points."<< endl;
}

void FeatureManager::triangulate(Vector3d Ps[], Matrix3d Rs[], Vector3d tic[], Matrix3d ric[])
{
    for (auto &it_per_id : feature)
    {
        // cout << "used_num = " << it_per_id.feature_per_frame.size() << endl;
        if (it_per_id.estimated_depth > 0)
            continue;
        it_per_id.used_num = it_per_id.feature_per_frame.size();

        if (!(it_per_id.used_num >= 2 && it_per_id.start_frame < WINDOW_SIZE - 2))
            continue;
        // if (it_per_id.used_num < 5)
        //     continue;

        // cout << "used_num = " << it_per_id.feature_per_frame.size() << endl;

        int imu_i = it_per_id.start_frame;

        // cout << "imu_i = "<< imu_i <<endl;

        Eigen::MatrixXd svd_A(4 * it_per_id.feature_per_frame.size(), 4);
        // Eigen::MatrixXd svd_A(4, 4);
        int svd_idx = 0;

        // triangulate with direct linear transformation (DLT)
        // see also: http://bardsley.org.uk/wp-content/uploads/2007/02/3d-reconstruction-using-the-direct-linear-transform.pdf

        // Rs[i] = w_R_imu of i th frame in the window
        // Ps[i] = w_p_imu of i th frame in the window
        // ric[0] = imu_R_cam, camera extrinsics, ric.size() = NUM_CAM
        // tic[0] = imu_t_cam, camera extrinsics
        // t0 = world_t_cam_0
        // R0 = world_R_cam_0
        Eigen::Vector3d t0 = Ps[imu_i] + Rs[imu_i] * tic[0];
        Eigen::Matrix3d R0 = Rs[imu_i] * ric[0];

        for (auto &it_per_frame : it_per_id.feature_per_frame)
        {
            // auto &it_per_frame = it_per_id.feature_per_frame[0];
            // t1 = w_t_{w,ci}
            Eigen::Vector3d t1 = Ps[imu_i] + Rs[imu_i] * tic[0];
            // R1 = w_R_ci
            Eigen::Matrix3d R1 = Rs[imu_i] * ric[0];
            // t = c0_t_{c0,ci}
            Eigen::Vector3d t = R0.transpose() * (t1 - t0);
            // cout << "t norm: " << t.norm() << endl;
            // R = c0_R_ci
            Eigen::Matrix3d R = R0.transpose() * R1;
            // leftP = ci_T_c0
            Eigen::Matrix<double, 3, 4> leftP;
            leftP.leftCols<3>() = R.transpose();
            leftP.rightCols<1>() = -R.transpose() * t;
            Eigen::Vector2d f = it_per_frame.point.head(2); // point in normalized plane
            svd_A.row(svd_idx++) = f[0] * leftP.row(2) - leftP.row(0);
            svd_A.row(svd_idx++) = f[1] * leftP.row(2) - leftP.row(1);

            // rightP = (ci_right)_T_c0
            Eigen::Matrix<double, 3, 4> rightP;
            Eigen::Vector3d left_t_right = ric[0].transpose() * (tic[1] - tic[0]);
            Eigen::Matrix3d left_R_right = ric[0].transpose() * ric[1];

            rightP.leftCols<3>() = left_R_right.transpose() * leftP.leftCols<3>();
            rightP.rightCols<1>() = left_R_right.transpose() * leftP.rightCols<1>() - left_R_right.transpose() * left_t_right;

            // check
            // cout << "r_R_l = \n" << rightP.leftCols<3>() * leftP.leftCols<3>().transpose() << endl;
            // cout << "r_t_l = \n" << rightP.leftCols<3>()*(-leftP.leftCols<3>().transpose()*leftP.rightCols<1>()) + rightP.rightCols<1>() << endl;

            f = it_per_frame.point_right.head(2); // point in normalized plane
            svd_A.row(svd_idx++) = f[0] * rightP.row(2) - rightP.row(0);
            svd_A.row(svd_idx++) = f[1] * rightP.row(2) - rightP.row(1);

            imu_i++;

            // cout << "imu_i = "<< imu_i <<endl;
        }
        ROS_ASSERT(svd_idx == svd_A.rows());
        // about SVD:
        // https://www.cs.cornell.edu/courses/cs3220/2010sp/notes/svd.pdf
        // about the solution Ax=0:
        // http://www.maths.lth.se/matematiklth/personal/calle/datorseende13/notes/forelas3.pdf
        Eigen::Vector4d svd_V = Eigen::JacobiSVD<Eigen::MatrixXd>(svd_A, Eigen::ComputeThinV).matrixV().rightCols<1>();
        double depth = svd_V[2] / svd_V[3];

        // cout << "depth: " << depth << endl;

        if(depth > 0.1)
        {
            it_per_id.estimated_depth = depth;
        }
        else
        {
            it_per_id.estimated_depth = INIT_DEPTH;
        }
        

        // if (depth < 0.1)
        // {
        //     it_per_id.estimated_depth = INIT_DEPTH;
        // }
        // else
        // {
        //     it_per_id.estimated_depth = depth;
        // }
    }
}

void FeatureManager::removeOutlier()
{
    ROS_BREAK();
    int i = -1;
    for (auto it = feature.begin(), it_next = feature.begin();
         it != feature.end(); it = it_next)
    {
        it_next++;
        i += it->used_num != 0;
        if (it->used_num != 0 && it->is_outlier == true)
        {
            feature.erase(it);
        }
    }
}

void FeatureManager::removeBackShiftDepth(Eigen::Matrix3d marg_R, Eigen::Vector3d marg_P, Eigen::Matrix3d new_R, Eigen::Vector3d new_P)
{
    for (auto it = feature.begin(), it_next = feature.begin();
         it != feature.end(); it = it_next)
    {
        it_next++;

        if (it->start_frame != 0)
            it->start_frame--;
        else
        {
            Eigen::Vector3d uv_i = it->feature_per_frame[0].point;
            it->feature_per_frame.erase(it->feature_per_frame.begin());
            // if (it->feature_per_frame.size() < 2)
            if(it->feature_per_frame.size() == 0)
            {
                feature.erase(it);
                continue;
            }
            else
            {
                Eigen::Vector3d pts_i = uv_i * it->estimated_depth;
                Eigen::Vector3d w_pts_i = marg_R * pts_i + marg_P;
                Eigen::Vector3d pts_j = new_R.transpose() * (w_pts_i - new_P);
                double dep_j = pts_j(2);
                if (dep_j > 0)
                    it->estimated_depth = dep_j;
                else
                    it->estimated_depth = INIT_DEPTH;
            }
        }
        // remove tracking-lost feature after marginalize
        /*
        if (it->endFrame() < WINDOW_SIZE - 1)
        {
            feature.erase(it);
        }
        */
    }
}

void FeatureManager::removeBack()
{
    for (auto it = feature.begin(), it_next = feature.begin();
         it != feature.end(); it = it_next)
    {
        it_next++;

        if (it->start_frame != 0)
            it->start_frame--;
        else
        {
            it->feature_per_frame.erase(it->feature_per_frame.begin());
            if (it->feature_per_frame.size() == 0)
                feature.erase(it);
        }
    }
}

void FeatureManager::removeFront(int frame_count)
{
    for (auto it = feature.begin(), it_next = feature.begin(); it != feature.end(); it = it_next)
    {
        it_next++;

        if (it->start_frame == frame_count)
        {
            it->start_frame--;
        }
        else
        {
            int j = WINDOW_SIZE - 1 - it->start_frame;
            if (it->endFrame() < frame_count - 1)
                continue;
            it->feature_per_frame.erase(it->feature_per_frame.begin() + j);
            if (it->feature_per_frame.size() == 0)
                feature.erase(it);
        }
    }
}

double FeatureManager::compensatedParallax2(const FeaturePerId &it_per_id, int frame_count)
{
    //check the second last frame is keyframe or not
    //parallax betwwen seconde last frame and third last frame
    const FeaturePerFrame &frame_i = it_per_id.feature_per_frame[frame_count - 2 - it_per_id.start_frame];
    const FeaturePerFrame &frame_j = it_per_id.feature_per_frame[frame_count - 1 - it_per_id.start_frame];

    double ans = 0;
    Vector3d p_j = frame_j.point;

    double u_j = p_j(0);
    double v_j = p_j(1);

    Vector3d p_i = frame_i.point;
    Vector3d p_i_comp;

    //int r_i = frame_count - 2;
    //int r_j = frame_count - 1;
    //p_i_comp = ric[camera_id_j].transpose() * Rs[r_j].transpose() * Rs[r_i] * ric[camera_id_i] * p_i;
    p_i_comp = p_i;
    double dep_i = p_i(2);
    double u_i = p_i(0) / dep_i;
    double v_i = p_i(1) / dep_i;
    double du = u_i - u_j, dv = v_i - v_j;

    double dep_i_comp = p_i_comp(2);
    double u_i_comp = p_i_comp(0) / dep_i_comp;
    double v_i_comp = p_i_comp(1) / dep_i_comp;
    double du_comp = u_i_comp - u_j, dv_comp = v_i_comp - v_j;

    ans = max(ans, sqrt(min(du * du + dv * dv, du_comp * du_comp + dv_comp * dv_comp)));

    return ans;
}