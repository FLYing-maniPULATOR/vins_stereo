import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

FONT_SIZE = 14

data = np.genfromtxt("latency.csv", delimiter=' ',names=['l'])['l'][:]
data = data*1000

B=plt.boxplot(data)
whiskers = [item.get_ydata() for item in B['whiskers']]
lower_whisker = whiskers[0][1]
upper_whisker = whiskers[1][1]
print(lower_whisker, upper_whisker)

num_outlier = sum(i > upper_whisker for i in data)

print("size: %i" %len(data))
print("size outlier: %i"% num_outlier)
print("size > 2ms : %i" % sum(i > 2 for i in data))
print("size > 5ms : %i" % sum(i > 5 for i in data))

fig  = plt.figure("Latency",figsize=(6,4))
plt.title('Latency', fontsize=FONT_SIZE)
plt.boxplot(data, patch_artist=True, showfliers=False, notch=False)
plt.ylabel('Latency [ms]', fontsize=FONT_SIZE)
plt.xticks([1], [''])
plt.grid()
fig.savefig("latency", dpi=300)

fig  = plt.figure("Latency1",figsize=(6,4))
plt.title('Latency', fontsize=FONT_SIZE)
plt.boxplot(data, patch_artist=True, showfliers=True, notch=False)
plt.ylabel('Latency [ms]', fontsize=FONT_SIZE)
plt.xticks([1], [''])
plt.grid()
fig.savefig("latency_outlier", dpi=300)

