import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

FONT_SIZE = 14

ft = [65, 71, 67, 68, 69, 63, 68, 70, 80, 75]
vio = [17, 16, 24, 28, 20, 25, 19, 18, 30, 29]
pg = [23, 30, 35, 29, 28, 29, 30, 27, 26, 32]

sigma = np.add(np.add(ft,vio),pg)

data = [ft, vio, pg]

fig  = plt.figure("CPU_Usage",figsize=(6,4))
plt.title('CPU load of different ROS Nodes', fontsize=FONT_SIZE)
plt.boxplot(data, patch_artist=True, showfliers=True, notch=False)
plt.ylabel('CPU Load %', fontsize=FONT_SIZE)
plt.xticks([1,2,3], ['Tracking', 'Local BA', 'Loop Closure'], fontsize=FONT_SIZE)
plt.grid()
fig.savefig("CPU_Usage", dpi=300)

fig  = plt.figure("CPU_Usage_Sum",figsize=(6,4))
plt.title('CPU load of the whole system', fontsize=FONT_SIZE)
plt.boxplot(sigma, patch_artist=True, showfliers=True, notch=False)
plt.ylabel('CPU Load %', fontsize=FONT_SIZE)
plt.xticks([1], [''], fontsize=FONT_SIZE)
plt.grid()
fig.savefig("CPU_Usage_Sum", dpi=300)