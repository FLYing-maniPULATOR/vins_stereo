import numpy as np
import matplotlib.pyplot as plt

FONT_SIZE = 14
LINE_WIDTH = 0.8


DATA_SET = "V1_01_easy"
# DATA_SET = "V1_02_medium"
# DATA_SET = "V1_03_difficult"
# DATA_SET = "MH_01_easy"
# DATA_SET = "MH_02_easy"
# DATA_SET = "MH_03_medium"
# DATA_SET = "MH_04_difficult"
# DATA_SET = "MH_05_difficult"
# DATA_SET = "Ifa_lab"
# DATA_SET = "TUD_BAR2"

# DATA_SET = "Hohe-Str-DD"

# names = ['VINS-Stereo']
# paths = ['ground_truth.csv']

# names = ['Ground Truth', 'VINS-Stereo', 'VINS-Stereo-loop', 'VINS-Mono', 'VINS-Fusion', 'MSCKF']
# paths = ['ground_truth.csv', 'vins_stereo_aligned.csv', 'vins_stereo_loop_aligned.csv', 'vins_mono_aligned.csv',
#         'vins_fusion_aligned.csv', 'msckf_aligned.csv']

# names = ['VINS-Stereo-loop', 'VINS-Stereo']
# paths = ['vins_stereo_loop_aligned.csv', 'vins_stereo_aligned.csv']

names = ['Ground Truth', 'VINS-Stereo', 'VINS-Mono', 'MSCKF', 'VINS-Fusion']
paths = ['ground_truth.csv', 'vins_stereo_aligned.csv', 'vins_mono_aligned.csv', 'msckf_aligned.csv','vins_fusion_aligned.csv']

# names = ['Ground Truth', 'VINS-Stereo', 'VINS-Stereo-loop', 'VINS-Mono']
# paths = ['ground_truth.csv', 'vins_stereo_aligned.csv', 'vins_stereo_loop_aligned.csv', 'vins_mono_aligned.csv']

# names = ['Ground Truth', 'VINS-Stereo']
# paths = ['ground_truth.csv', 'vins_stereo_aligned.csv']

trajs = []

for name,path in zip(names,paths):
    trajs.append(np.genfromtxt(path, delimiter=' ',names=['timestamp','x', 'y', 'z']))

traj_temp = trajs[0]
traj_length = 0
for i in range(traj_temp['x'].size -1):
    p1 = np.array([traj_temp['x'][i],traj_temp['y'][i],traj_temp['z'][i]])
    p2 = np.array([traj_temp['x'][i+1],traj_temp['y'][i+1],traj_temp['z'][i+1]])
    traj_length += np.linalg.norm(p1-p2)
print('Trajectory length: %f' % traj_length)

fig = plt.figure("Trajectory_xy",figsize=(8,6))
for name, traj in zip(names, trajs):
    plt.plot(traj['x'],traj['y'], label=name, linewidth=LINE_WIDTH)
plt.xlabel('x [m]', fontsize = FONT_SIZE)
plt.ylabel('y [m]', fontsize = FONT_SIZE)
plt.axis('equal')
plt.grid()
plt.legend(loc='best', shadow=False, fontsize = FONT_SIZE)
plt.title('Trajectory of '+DATA_SET + "  " + "length = %.1f m"%traj_length, fontsize = FONT_SIZE)
plt.tight_layout()
fig.savefig(DATA_SET+"_Trajectories_xy", dpi=300)

fig = plt.figure("Trajectory_zy",figsize=(8,6))
for name, traj in zip(names, trajs):
    plt.plot(traj['y'],traj['z'], label=name, linewidth=LINE_WIDTH)
plt.xlabel('y [m]', fontsize = FONT_SIZE)
plt.ylabel('z [m]', fontsize = FONT_SIZE)
plt.axis('equal')
plt.grid()
plt.legend(loc='best', shadow=False, fontsize = FONT_SIZE)
plt.title('Trajectory of '+DATA_SET +"  " + "length = %.1f m"%traj_length, fontsize = FONT_SIZE)
plt.tight_layout()
fig.savefig(DATA_SET+"_Trajectories_yz", dpi=300)