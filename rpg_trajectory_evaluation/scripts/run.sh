# python2 analyze_trajectories.py \
#   --platform intel_nuc --odometry_error --overall_odometry_error --plot_trajectories --rmse_table

# python2 analyze_trajectory_single.py \
# ../results/intel_nuc/okvis/intel_nuc_okvis_MH01

alg_name=vins_fusion

python2 analyze_trajectory_single.py \
../results/intel_nuc/${alg_name}/intel_nuc_${alg_name}_MH01

python2 analyze_trajectory_single.py   \
../results/intel_nuc/${alg_name}/intel_nuc_${alg_name}_MH02

python2 analyze_trajectory_single.py   \
../results/intel_nuc/${alg_name}/intel_nuc_${alg_name}_MH03

python2 analyze_trajectory_single.py   \
../results/intel_nuc/${alg_name}/intel_nuc_${alg_name}_MH04

python2 analyze_trajectory_single.py   \
../results/intel_nuc/${alg_name}/intel_nuc_${alg_name}_MH05

python2 analyze_trajectory_single.py   \
../results/intel_nuc/${alg_name}/intel_nuc_${alg_name}_V1_01

python2 analyze_trajectory_single.py   \
../results/intel_nuc/${alg_name}/intel_nuc_${alg_name}_V1_02

python2 analyze_trajectory_single.py   \
../results/intel_nuc/${alg_name}/intel_nuc_${alg_name}_V1_03