#!/bin/bash

result_path=./results

datasets=(MH01 MH02 MH03 MH04 MH05 V1_01 V1_02 V1_03)

gt_name=groundtruth

alg_names=( msckf vins_fusion vins_mono vins_stereo_loop vins_stereo_no_loop okvis)

platform=intel_nuc

cfg_file=eval_cfg.yaml

for alg_name in "${alg_names[@]}"
do
mkdir -p $result_path/$platform/$alg_name
for dataset in "${datasets[@]}"
do
out_path=$result_path/$platform/$alg_name/${platform}_${alg_name}_${dataset}
mkdir -p $out_path
cp ./$cfg_file $out_path/$cfg_file
python2 asl_groundtruth_to_pose.py --output ../$out_path/stamped_groundtruth.txt ./$dataset/$gt_name.csv
python2 asl_groundtruth_to_pose.py --output ../$out_path/stamped_traj_estimate.txt ./$dataset/$alg_name.csv
done

done
