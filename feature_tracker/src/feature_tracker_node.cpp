#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/Imu.h>
#include <std_msgs/Bool.h>
#include <cv_bridge/cv_bridge.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>

#include "feature_tracker.h"

#include <mutex>
#include <condition_variable>
#include <thread>

#define SHOW_UNDISTORTION 0

vector<uchar> r_status;
vector<float> r_err;
queue<pair<sensor_msgs::ImageConstPtr,sensor_msgs::ImageConstPtr>> img_buf;
queue<sensor_msgs::ImuConstPtr> imu_buf;

ros::Publisher pub_img,pub_match;
ros::Publisher pub_restart;

FeatureTracker trackerData[NUM_OF_CAM];
double first_image_time;
int pub_count = 1;
int pub_features_num = 0;
bool first_image_flag = true;
double last_image_time = 0;
bool init_pub = 0;

double curr_img_time;
double curr_imu_time;

double image_frame_rate = 20;
double imu_frame_rate = 200;

std::mutex m_buf;
std::condition_variable cond_var;

template <typename Derived>
Eigen::Matrix<typename Derived::Scalar, 3, 3> skewSymmetric(const Eigen::MatrixBase<Derived> &q)
{
    Eigen::Matrix<typename Derived::Scalar, 3, 3> ans;
    ans << typename Derived::Scalar(0), -q(2), q(1),
        q(2), typename Derived::Scalar(0), -q(0),
        -q(1), q(0), typename Derived::Scalar(0);
    return ans;
}

void imu_callback(const sensor_msgs::ImuConstPtr &imu_msg)
{
    // static double ros_last_time  = 0;
    // cout << "Dt: " << ros::Time::now().toSec() - ros_last_time << endl;
    // ros_last_time = ros::Time::now().toSec();

    // TicToc t_buf;
    curr_imu_time = imu_msg->header.stamp.toSec();

    static double last_time = 0;

    if (imu_msg->header.stamp.toSec() <= last_time)
    {
        ROS_WARN("imu message in disorder!");
        last_time = imu_msg->header.stamp.toSec();
        return;
    }

    if(last_time != 0)
    {
        // double dt = ros::Time::now().toSec() - last_time;
        double dt = imu_msg->header.stamp.toSec() - last_time;
        // cout << "imu dt: " << dt << endl;
        imu_frame_rate = 0.8*imu_frame_rate + 0.2*(1.0/dt);
    }
    last_time = imu_msg->header.stamp.toSec();
    // last_time = ros::Time::now().toSec();

    m_buf.lock();
    imu_buf.push(imu_msg);
    m_buf.unlock();
    cond_var.notify_one();

    // cout << "IMU callback time: " << t_buf.toc() << " ms" << endl;
    // cond_var.notify_one();
    // cout << "imu call back time: " << imu_msg->header.stamp.toSec() - last_time << endl;
    // cout << "get imu" << endl;
}

void img_callback(const sensor_msgs::ImageConstPtr &img_msg_0,
                  const sensor_msgs::ImageConstPtr &img_msg_1)
{
    curr_img_time =  img_msg_0->header.stamp.toSec();

    static double last_time = 0;

    if(last_time != 0)
    {
        double dt = img_msg_0->header.stamp.toSec() - last_time;
        // cout << "image dt: " << dt << endl;
        image_frame_rate = 0.8*image_frame_rate + 0.2*(1.0/dt);
    }
    last_time = img_msg_0->header.stamp.toSec();

    m_buf.lock();
    img_buf.push(make_pair(img_msg_0, img_msg_1));
    // if(!imu_buf.empty())
    //     cout << "img - imu t: "<< img_msg_0->header.stamp.toSec() - imu_buf.back()->header.stamp.toSec() << endl;
    m_buf.unlock();
    cond_var.notify_one();

    // cout << "curr img - curr imu time: " << 
    // (curr_img_time - curr_imu_time)*1000 << "ms" << endl;

    // cout << "get image" << endl;

    // static double last_time = 0;
    // cout << "img call back time: " << img_msg_0->header.stamp.toSec() - last_time << endl;
    // last_time = img_msg_0->header.stamp.toSec();
}


void processImage(const sensor_msgs::ImageConstPtr &img_msg_0,
                  const sensor_msgs::ImageConstPtr &img_msg_1,
                  const cv::Matx33f &c_R_p)
{
    if(first_image_flag)
    {
        first_image_flag = false;
        first_image_time = img_msg_0->header.stamp.toSec();
        last_image_time = img_msg_0->header.stamp.toSec();
        return;
    }
    // detect unstable camera stream
    if (img_msg_0->header.stamp.toSec() - last_image_time > 1.0 || img_msg_0->header.stamp.toSec() < last_image_time)
    {
        ROS_WARN("image discontinue! reset the feature tracker!");
        first_image_flag = true; 
        last_image_time = 0;
        pub_count = 1;
        std_msgs::Bool restart_flag;
        restart_flag.data = true;
        pub_restart.publish(restart_flag);
        return;
    }
    
    // frequency control
    if (round(1.0 * pub_count / (img_msg_0->header.stamp.toSec() - first_image_time)) <= FREQ)
    {
        PUB_THIS_FRAME = true;
        // reset the frequency control
        if (abs(1.0 * pub_count / (img_msg_0->header.stamp.toSec() - first_image_time) - FREQ) < 0.01 * FREQ)
        {
            first_image_time = img_msg_0->header.stamp.toSec();
            pub_count = 0;
        }
    }
    else
        PUB_THIS_FRAME = false;

    cv_bridge::CvImageConstPtr ptr_img_0; // pointer to left raw image
    cv_bridge::CvImageConstPtr ptr_img_1; // pointer to right raw image
    
    // copy left raw image
    if (img_msg_0->encoding == "8UC1")
    {
        sensor_msgs::Image img;
        img.header = img_msg_0->header;
        img.height = img_msg_0->height;
        img.width = img_msg_0->width;
        img.is_bigendian = img_msg_0->is_bigendian;
        img.step = img_msg_0->step;
        img.data = img_msg_0->data;
        img.encoding = "mono8";
        ptr_img_0 = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
    }
    else
        ptr_img_0 = cv_bridge::toCvCopy(img_msg_0, sensor_msgs::image_encodings::MONO8);

    // copy right raw image
    if (img_msg_1->encoding == "8UC1")
    {
        sensor_msgs::Image img;
        img.header = img_msg_1->header;
        img.height = img_msg_1->height;
        img.width = img_msg_1->width;
        img.is_bigendian = img_msg_1->is_bigendian;
        img.step = img_msg_1->step;
        img.data = img_msg_1->data;
        img.encoding = "mono8";
        ptr_img_1 = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);
    }
    else
        ptr_img_1 = cv_bridge::toCvCopy(img_msg_1, sensor_msgs::image_encodings::MONO8);

    TicToc t_r; // stop watch tracker processing time
//     for (int i = 0; i < NUM_OF_CAM; i++)
//     {
//         ROS_DEBUG("processing camera %d", i);
//         if (i != 1 || !STEREO_TRACK)
//             trackerData[i].readImage(ptr_img_0->image.rowRange(ROW * i, ROW * (i + 1)), img_msg_0->header.stamp.toSec());
//         else
//         {
//             if (EQUALIZE)
//             {
//                 cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
//                 clahe->apply(ptr_img_0->image.rowRange(ROW * i, ROW * (i + 1)), trackerData[i].cur_img);
//             }
//             else
//                 trackerData[i].cur_img = ptr_img_0->image.rowRange(ROW * i, ROW * (i + 1));
//         }

// #if SHOW_UNDISTORTION
//         trackerData[i].showUndistortion("undistrotion_" + std::to_string(i));
// #endif
//     }
    last_image_time = img_msg_0->header.stamp.toSec();
    
    cv::Mat img0 = ptr_img_0->image.clone();
    cv::Mat img1 = ptr_img_1->image.clone();

    // equalize raw images from cam0 and cam1
    if (EQUALIZE)
    {
        cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE(3.0, cv::Size(3, 3));
        TicToc t_c;
        clahe->apply(img0, img0);
        clahe->apply(img1, img1);
        ROS_DEBUG("CLAHE costs: %fms", t_c.toc());
    }

    trackerData[0].readImage(img0, img1, img_msg_0->header.stamp.toSec(), c_R_p);

    for (unsigned int i = 0;; i++)
    {
        bool completed = false;
        for (int j = 0; j < NUM_OF_CAM; j++)
            if (j != 1 || !STEREO_TRACK)
                completed |= trackerData[j].updateID(i);
        if (!completed)
            break;
    }

   if (PUB_THIS_FRAME)
   {
        pub_count++;

        // if(trackerData[0].cur_pts_cam0.size() >= 8)
        {
            sensor_msgs::PointCloudPtr feature_points(new sensor_msgs::PointCloud);
            sensor_msgs::ChannelFloat32 id_of_point;
            sensor_msgs::ChannelFloat32 u_of_point_cam0;
            sensor_msgs::ChannelFloat32 v_of_point_cam0;
            sensor_msgs::ChannelFloat32 velocity_x_of_point_cam0;
            sensor_msgs::ChannelFloat32 velocity_y_of_point_cam0;
            sensor_msgs::ChannelFloat32 x_of_point_cam1;
            sensor_msgs::ChannelFloat32 y_of_point_cam1;
            sensor_msgs::ChannelFloat32 z_of_point_cam1;
            sensor_msgs::ChannelFloat32 u_of_point_cam1;
            sensor_msgs::ChannelFloat32 v_of_point_cam1;

            feature_points->header = img_msg_0->header;
            feature_points->header.frame_id = "world";

            vector<set<int>> hash_ids(NUM_OF_CAM);

            pub_features_num = 0;

            for (int i = 0; i < NUM_OF_CAM; i++)
            {
                auto &un_pts_cam0 = trackerData[i].cur_un_pts_cam0;
                auto &un_pts_cam1 = trackerData[i].cur_un_pts_cam1;
                auto &cur_pts_cam0 = trackerData[i].cur_pts_cam0;
                auto &cur_pts_cam1 = trackerData[i].cur_pts_cam1;
                auto &ids = trackerData[i].ids;
                auto &pts_velocity_cam0 = trackerData[i].pts_velocity_cam0;

                for (unsigned int j = 0; j < ids.size(); j++)
                {
                    // maximum feature num for update
                    // TODO: add to yaml
                    if(pub_features_num >= 180)
                        break;

                    if (trackerData[i].track_cnt[j] >= MIN_TRACKED_CNT)
                    {
                        int p_id = ids[j];
                        hash_ids[i].insert(p_id);
                        geometry_msgs::Point32 point_cam0, point_cam1;
                        // points in normalized plane
                        point_cam0.x = un_pts_cam0[j].x;
                        point_cam0.y = un_pts_cam0[j].y;
                        point_cam0.z = 1;
                        point_cam1.x = un_pts_cam1[j].x;
                        point_cam1.y = un_pts_cam1[j].y;
                        point_cam1.z = 1;

                        feature_points->points.push_back(point_cam0); // 3d point in normalised plane
                        id_of_point.values.push_back(p_id * NUM_OF_CAM + i);
                        u_of_point_cam0.values.push_back(cur_pts_cam0[j].x);
                        v_of_point_cam0.values.push_back(cur_pts_cam0[j].y);
                        velocity_x_of_point_cam0.values.push_back(pts_velocity_cam0[j].x);
                        velocity_y_of_point_cam0.values.push_back(pts_velocity_cam0[j].y);

                        u_of_point_cam1.values.push_back(cur_pts_cam1[j].x);
                        v_of_point_cam1.values.push_back(cur_pts_cam1[j].y);
                        x_of_point_cam1.values.push_back(point_cam1.x);
                        y_of_point_cam1.values.push_back(point_cam1.y);
                        z_of_point_cam1.values.push_back(point_cam1.z);

                        pub_features_num++;
                    }
                }
            }

            static double pub_features_num_avg = 0;
            // apply low pass
            pub_features_num_avg = 0.85*pub_features_num_avg + 0.15*pub_features_num;

            // cout << "pub_features_num_avg = " << pub_features_num_avg
            // << " EQUALIZE = " << EQUALIZE << endl;

            if(pub_features_num_avg < 50)
                EQUALIZE = 1;
            else if(pub_features_num_avg > 90)
                EQUALIZE = 0;
            

            // if(pub_features_num < 50)
            // {
            //     MAX_CNT+=50;
            // }
            // else if(pub_features_num > 120)
            // {
            //     MAX_CNT -= 25;
            // }

            // MAX_CNT = max(50, min(MAX_CNT, 200));

            // cout << "MAX_CNT = " << MAX_CNT << endl;

            feature_points->channels.push_back(id_of_point); //[0] id of feature
            feature_points->channels.push_back(u_of_point_cam0);  //[1] x position in image plane
            feature_points->channels.push_back(v_of_point_cam0);  //[2] y position in image plane
            feature_points->channels.push_back(velocity_x_of_point_cam0); //[3] velocity in normalised plane
            feature_points->channels.push_back(velocity_y_of_point_cam0);//[4] 
            
            feature_points->channels.push_back(x_of_point_cam1); //[5]
            feature_points->channels.push_back(y_of_point_cam1); //[6]
            feature_points->channels.push_back(z_of_point_cam1); //[7]
            feature_points->channels.push_back(u_of_point_cam1); //[8]
            feature_points->channels.push_back(v_of_point_cam1); //[9]
            ROS_DEBUG("publish %f, at %f", feature_points->header.stamp.toSec(), ros::Time::now().toSec());
            // skip the first image; since no optical speed on frist image
            if (!init_pub)
            {
                init_pub = 1;
            }
            else
                pub_img.publish(feature_points);
        }
        if (SHOW_TRACK)
        {
            cv::Mat stereo_img;
            cv::hconcat(ptr_img_0->image, ptr_img_1->image, stereo_img);
            cv::cvtColor(stereo_img, stereo_img, CV_GRAY2RGB);

            cv::Mat tmp_img = stereo_img.colRange(0, COL);
        
            for (unsigned int j = 0; j < trackerData[0].cur_pts_cam0.size(); j++)
            {
                if (trackerData[0].track_cnt[j] < MIN_TRACKED_CNT)
                    continue;

                Vector2d tmp_cur_un_pts_cam0 (trackerData[0].cur_un_pts_cam0[j].x, trackerData[0].cur_un_pts_cam0[j].y);
                Vector2d tmp_cur_un_pts_cam1 (trackerData[0].cur_un_pts_cam1[j].x, trackerData[0].cur_un_pts_cam1[j].y);
                double disparity = (tmp_cur_un_pts_cam0 - tmp_cur_un_pts_cam1).norm() * FOCAL_LENGTH;
                double depth = 0.12*FOCAL_LENGTH/disparity;
                // std::cout << "disparity = " << disparity << std::endl;
                // std::cout << "depth = " << depth << std::endl;
                // std::cout << "[x, y] = " << FOCAL_LENGTH*(tmp_cur_un_pts_cam0 - tmp_cur_un_pts_cam1).transpose() << std::endl;
                //draw speed line
                Vector2d tmp_pts_velocity (trackerData[0].pts_velocity_cam0[j].x, trackerData[0].pts_velocity_cam0[j].y);
                Vector3d tmp_prev_un_pts;
                tmp_prev_un_pts.head(2) = tmp_cur_un_pts_cam0 - 0.10 * tmp_pts_velocity;
                tmp_prev_un_pts.z() = 1;
                // Vector3d tmp_prev_uv;
                cv::Point2f tmp_prev_uv;
                tmp_prev_uv = trackerData[0].distortPoint(cv::Point2f(tmp_prev_un_pts.x(), tmp_prev_un_pts.y()), 
                                trackerData[0].model_cam0.K, trackerData[0].model_cam0.model_name, trackerData[0].model_cam0.D);
                // trackerData[0].m_camera_0->spaceToPlane(tmp_prev_un_pts, tmp_prev_uv);
                // cv::line(tmp_img, trackerData[0].cur_pts_cam0[j], cv::Point2f(tmp_prev_uv.x(), tmp_prev_uv.y()), cv::Scalar(0 , 255, 0), 1 , 8, 0);

                double len = std::min(1.0, 1.0 * trackerData[0].track_cnt[j] / WINDOW_SIZE);

                if(depth > 3.8)
                    cv::circle(tmp_img, trackerData[0].cur_pts_cam0[j], 2, cv::Scalar(255 * (1 - len), 0, 255 * len), 2);
                else
                    cv::circle(tmp_img, trackerData[0].cur_pts_cam0[j], 2, cv::Scalar(255 * (1 - len), 255 * len, 0), 2);

                cv::line(tmp_img, trackerData[0].cur_pts_cam0[j], tmp_prev_uv, cv::Scalar(0 , 255, 0), 1 , 8, 0);
                
                // char name[10];
                // sprintf(name, "%d", trackerData[0].ids[j]);
                // cv::putText(tmp_img, name, trackerData[0].cur_pts_cam0[j], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));
            }

            tmp_img = stereo_img.colRange(COL, 2 * COL);
            for (unsigned int j = 0; j < trackerData[0].cur_pts_cam1.size(); j++)
            {
                if (trackerData[0].track_cnt[j] < MIN_TRACKED_CNT)
                    continue;

                Vector2d tmp_cur_un_pts_cam0(trackerData[0].cur_un_pts_cam0[j].x, trackerData[0].cur_un_pts_cam0[j].y);
                Vector2d tmp_cur_un_pts_cam1(trackerData[0].cur_un_pts_cam1[j].x, trackerData[0].cur_un_pts_cam1[j].y);

                // Eigen::Matrix<double, 3, 4> pose_left, pose_right;
                // Eigen::Vector3d point_3d;
                // pose_left.leftCols(3) = Eigen::Matrix3d::Identity();
                // pose_left.rightCols(1).setZero();
                // // pose_right.leftCols(3) = Eigen::Matrix3d::Identity();
                // // pose_right.rightCols(1) = Eigen::Vector3d(0.12,0,0);
                // pose_right << 0.99997, -0.00893, 0.00273, -0.11981,
                //     0.00895, 0.99997, 0.00224, -0.00083,
                //     -0.00273, -0.00221, 1.00000, 0.00010;
                // FeatureTracker::triangulatePoint(pose_left, pose_right, tmp_cur_un_pts_cam0, tmp_cur_un_pts_cam1, point_3d);

                double disparity = (tmp_cur_un_pts_cam0 - tmp_cur_un_pts_cam1).norm() * FOCAL_LENGTH;
                double depth = 0.12*FOCAL_LENGTH/disparity;
                // double depth = point_3d(2);

                double len = std::min(1.0, 1.0 * trackerData[0].track_cnt[j] / WINDOW_SIZE);
                if (depth > 3.8)
                    cv::circle(tmp_img, trackerData[0].cur_pts_cam1[j], 2, cv::Scalar(255 * (1 - len), 0, 255 * len), 2);
                else
                    cv::circle(tmp_img, trackerData[0].cur_pts_cam1[j], 2, cv::Scalar(255 * (1 - len), 255 * len, 0), 2);
                // char name[10];
                // sprintf(name, "%d", trackerData[0].ids[j]);
                // cv::putText(tmp_img, name, trackerData[0].cur_pts_cam1[j], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));

                // char name[15];
                // sprintf(name, "%.1fm", depth);
                // cv::putText(tmp_img, name, trackerData[0].cur_pts_cam1[j], cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0));
            }

            char image_frame_rate_str[20];
            char imu_frame_rate_str[20];
            char tracked_features_num_str[20];
            char equalize_status_str[20];
            sprintf(image_frame_rate_str, "Image fps: %.1f", image_frame_rate);
            sprintf(imu_frame_rate_str, "IMU fps: %.1f", imu_frame_rate);
            sprintf(tracked_features_num_str, "Tracked fts: %d", pub_features_num);
            sprintf(equalize_status_str, " EQUALIZE: %s", (EQUALIZE == 1)?"ON":"OFF");

            cv::putText(stereo_img, image_frame_rate_str, cv::Point2f(10, 25), 
            cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0, 255, 0), 2.0);

            cv::putText(stereo_img, imu_frame_rate_str, cv::Point2f(COL+10, 25), 
            cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0, 255, 0), 2.0);

            cv::putText(stereo_img, tracked_features_num_str, cv::Point2f(10, ROW-25), 
            cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0, 255, 0), 2.0);

            cv::putText(stereo_img, equalize_status_str, cv::Point2f(COL+10, ROW-25), 
            cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0, 255, 0), 2.0);

            cv::resize(stereo_img, stereo_img, cv::Size(2*COL/1.5, ROW/1.5));
            // cv::imshow("stereo_debug", stereo_img);
            // cv::waitKey(1);
            cv_bridge::CvImage match_img_msg;
            match_img_msg.encoding = sensor_msgs::image_encodings::TYPE_8UC3; 
            match_img_msg.image    = stereo_img;
            // pub_match.publish(ptr_img_0->toImageMsg());
            pub_match.publish(match_img_msg);
        }
    }
    ROS_DEBUG("whole feature tracker processing costs: %f", t_r.toc());
}

vector<pair<vector<sensor_msgs::ImuConstPtr>, pair<sensor_msgs::ImageConstPtr,sensor_msgs::ImageConstPtr>>> measurements;

bool getMeasurement(queue<pair<sensor_msgs::ImageConstPtr,sensor_msgs::ImageConstPtr>> &img_buf,
                    queue<sensor_msgs::ImuConstPtr>& imu_buf)
{

    // cout << "start: img_time - imu back: "<< img_buf.front().first->header.stamp.toSec() - imu_buf.back()->header.stamp.toSec() << endl;

    measurements.clear();

    while(!img_buf.empty() && !imu_buf.empty())
    {
        // cout << "imu buf size : " <<imu_buf.size() << endl;
        // cout << "img buf size : " <<img_buf.size() << endl;

        auto img_msg = img_buf.front();

        if(imu_buf.back()->header.stamp.toSec() < img_msg.first->header.stamp.toSec() - 0.005)
            return false;
    
        std::vector<sensor_msgs::ImuConstPtr> IMUs;
        // cout << "left time - right time: " << img_msg.first->header.stamp.toSec() - img_msg.second->header.stamp.toSec() << endl;
        // cout << "imu buf size (before): " <<imu_buf.size() << endl;
        // cout << "img_time - imu front: "<< img_left_msg->header.stamp.toSec() - imu_buf.front()->header.stamp.toSec() << endl;
        while(!imu_buf.empty() && imu_buf.front()->header.stamp.toSec() < img_msg.first->header.stamp.toSec())
        {
            IMUs.push_back(imu_buf.front());
            // cout << "img_time - imu front: " << img_msg.first->header.stamp.toSec() - imu_buf.front()->header.stamp.toSec() << endl;
            imu_buf.pop();
        }
        // cout << "imu buf size (after): " <<imu_buf.size() << endl;
        // cout << "IMUs size: " <<IMUs.size() << endl;

        if(!IMUs.empty())
            measurements.emplace_back(IMUs, img_msg);

        img_buf.pop();
    }

    if(!measurements.empty())
        return true;   
    else
        return false;  
}

Eigen::Matrix3d imuPredictRot(const vector<sensor_msgs::ImuConstPtr> &imus, const double current_img_time)
{
    int i = 0;
    double wx = 0, wy = 0, wz = 0;
    double last_t, curr_t, dt;
    double t_sum = 0;

    // Eigen::Vector3d gyro_bias(0.000670835,0.0200407,0.0770659); // euroc
    // Eigen::Vector3d gyro_bias(0.000849913, -0.000502744, 0.000937448);//mynteye
    Eigen::Vector3d gyro_bias = Eigen::Vector3d::Zero();

    TicToc t_p;

    Eigen::Matrix3d old_R_new = Eigen::Matrix3d::Identity();
    
    for(auto &&imu:imus)
    {
        curr_t = imu->header.stamp.toSec();

        if(i == 0) // first frame
        {
            wx = imu->angular_velocity.x;
            wy = imu->angular_velocity.y;
            wz = imu->angular_velocity.z;
            last_t = imu->header.stamp.toSec();
            i++;
            continue;
        };

        dt = curr_t - last_t;
        
        // cout << "dt: " << dt << endl;
        Eigen::Vector3d d_w = (Eigen::Vector3d(wx, wy, wz) - gyro_bias)*dt;
        Eigen::Matrix3d d_R = Eigen::Matrix3d::Identity() + skewSymmetric(d_w);
        old_R_new = old_R_new*d_R;
        
        t_sum += dt;

        wx = imu->angular_velocity.x;
        wy = imu->angular_velocity.y;
        wz = imu->angular_velocity.z;

        last_t = curr_t;
        i++;
    }

    dt = current_img_time - last_t;
    // cout << "dt: " << dt << endl;
    Eigen::Vector3d d_w = (Eigen::Vector3d(wx, wy, wz) - gyro_bias)*dt;
    Eigen::Matrix3d d_R = Eigen::Matrix3d::Identity() + skewSymmetric(d_w);
    old_R_new = old_R_new*d_R; // b_{k}_R_b_{k+1}
    t_sum += dt;

    Eigen::Matrix3d imu_R_cam0;
    
    // cv::cv2eigen(IMU_T_CAM0(cv::Range(0, 3), cv::Range(0, 3)), imu_R_cam0); // super slow

    imu_R_cam0 << IMU_T_CAM0.at<double>(0,0),  IMU_T_CAM0.at<double>(0,1),  IMU_T_CAM0.at<double>(0,2),
                  IMU_T_CAM0.at<double>(1,0),  IMU_T_CAM0.at<double>(1,1),  IMU_T_CAM0.at<double>(1,2),
                  IMU_T_CAM0.at<double>(2,0),  IMU_T_CAM0.at<double>(2,1),  IMU_T_CAM0.at<double>(2,2);

    // cout << "imu_R_cam0" << imu_R_cam0 << endl;
   
    // cout << "t sum: " << t_sum << endl;
    // cout << "old_R_new: \n" << old_R_new << endl;

    Eigen::Matrix3d new_R_old; // c_{k+1}_R_c_{k}
    new_R_old = imu_R_cam0.transpose()*old_R_new.transpose()*imu_R_cam0;

    // cout << "new_R_old: \n" << new_R_old << endl;
    //  cout << "IMU predict time: " << t_p.toc() << endl;
    return new_R_old;
}


void process()
{
    while(true)
    {
        std::unique_lock<std::mutex> lk(m_buf);
        cond_var.wait(lk, [&]{ 
            return getMeasurement(img_buf,imu_buf); 
            });
        lk.unlock();
        // cout << "measurements size: " << measurements.size() << endl;
        // cout << "IMUs size: " << measurements[0].first.size() << endl;
        // cout << "start process" << endl;
        for(auto &measurement:measurements)
        {
            // cout << "IMUs size: " << measurement.first.size() << endl;
            TicToc t_tr;
            TicToc t_imu;
            auto &img_msg_left = measurement.second.first;
            auto &img_msg_right = measurement.second.second;
            auto &imus = measurement.first;
            Eigen::Matrix3d cam0_p_R_c = Eigen::Matrix3d::Identity();
            cam0_p_R_c = imuPredictRot(imus, img_msg_left->header.stamp.toSec());
            // cv::Mat c_R_p;
            // double *data = cam0_p_R_c.data();
            // cv::eigen2cv(cam0_p_R_c, c_R_p); // this is super slow why?????
            // cv::Matx33f c_R_p_x(c_R_p(cv::Rect(0,0,3,3)));
            cv::Matx33f c_R_p_x(cam0_p_R_c(0,0), cam0_p_R_c(0,1), cam0_p_R_c(0,2),
                                cam0_p_R_c(1,0), cam0_p_R_c(1,1), cam0_p_R_c(1,2),
                                cam0_p_R_c(2,0), cam0_p_R_c(2,1), cam0_p_R_c(2,2));
            // cout << "Tracking time: "<< t_imu.toc() << endl;
            // cout << "cam0_p_R_c: " << cam0_p_R_c << endl;
            // cout << "c_R_p_x: " << c_R_p_x << endl;sss
            // cout << "img_time - imu back: "<< img_msg_left->header.stamp.toSec() - imus.back()->header.stamp.toSec() << endl;
            processImage(img_msg_left, img_msg_right, c_R_p_x);
            // static long int frame_cnt = 0;
            // static double t_sum = 0;
            // frame_cnt ++;
            // t_sum += t_tr.toc();
            // cout << "avg. Tracking time: "<< t_sum/frame_cnt << endl;
            
        }
        // cout << "end process" << endl;
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "feature_tracker");
    ros::NodeHandle n("~");
    ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info);
    readParameters(n);

    // for (int i = 0; i < NUM_OF_CAM; i++)
    //     trackerData[i].readIntrinsicParameterCam0(CAM_NAMES[i]);

    // load camera model for left camera
    // trackerData[0].readIntrinsicParameter(CAM0_MODEL, trackerData[0].m_camera_0);
    // load camera model for right camera
    // trackerData[0].readIntrinsicParameter(CAM0_MODEL, trackerData[0].m_camera_1);

    trackerData[0].model_cam0.model_name = CAM0_MODEL_NAME;
    trackerData[0].model_cam0.K = CAM0_MODEL_K;
    trackerData[0].model_cam0.D = CAM0_MODEL_D;

    trackerData[0].model_cam1.model_name = CAM1_MODEL_NAME;
    trackerData[0].model_cam1.K = CAM1_MODEL_K;
    trackerData[0].model_cam1.D = CAM1_MODEL_D;

    if(FISHEYE)
    {
        for (int i = 0; i < NUM_OF_CAM; i++)
        {
            trackerData[i].fisheye_mask = cv::imread(FISHEYE_MASK, 0);
            if(!trackerData[i].fisheye_mask.data)
            {
                ROS_INFO("load mask fail");
                ROS_BREAK();
            }
            else
                ROS_INFO("load mask success");
        }
    }

    // ros::Subscriber sub_img = n.subscribe(IMAGE_TOPIC, 2000, img_l_callback);
    // cout << "IMAGE_TOPIC: " << IMAGE_TOPIC << endl;
    // cout << "IMU_TOPIC: " << IMU_TOPIC << endl;
    
    message_filters::Subscriber<sensor_msgs::Image> cam0_img_sub(n,IMAGE_TOPIC,100);
    message_filters::Subscriber<sensor_msgs::Image> cam1_img_sub(n,IMAGE_TOPIC_R,100);
    message_filters::TimeSynchronizer<sensor_msgs::Image, sensor_msgs::Image>stereo_sub(100);
    stereo_sub.connectInput(cam0_img_sub, cam1_img_sub);
    stereo_sub.registerCallback(boost::bind(&img_callback, _1, _2));

    ros::Subscriber sub_imu = n.subscribe(IMU_TOPIC, 1000, imu_callback, ros::TransportHints().tcpNoDelay());

    pub_img = n.advertise<sensor_msgs::PointCloud>("feature", 100);
    pub_match = n.advertise<sensor_msgs::Image>("feature_img",100);
    pub_restart = n.advertise<std_msgs::Bool>("restart",100);
    /*
    if (SHOW_TRACK)
        cv::namedWindow("vis", cv::WINDOW_NORMAL);
    */
    std::thread tracking_process{process};
    ros::spin();
    return 0;
}


// new points velocity is 0, pub or not?
// track cnt > 1 pub?