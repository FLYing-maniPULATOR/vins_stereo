#include "feature_tracker.h"
#include <random_numbers/random_numbers.h>

int FeatureTracker::n_id = 0;

// return TRUE if point in border
bool inBorder(const cv::Point2f &pt)
{
    const int BORDER_SIZE = 1;
    int img_x = cvRound(pt.x);
    int img_y = cvRound(pt.y);
    return BORDER_SIZE <= img_x && img_x < COL - BORDER_SIZE && BORDER_SIZE <= img_y && img_y < ROW - BORDER_SIZE;
}

void reduceVector(vector<cv::Point2f> &v, vector<uchar> status)
{
    if(v.size()==0) return;
    int j = 0;
    for (int i = 0; i < int(v.size()); i++)
        if (status[i])
            v[j++] = v[i];
    v.resize(j);
}

void reduceVector(vector<int> &v, vector<uchar> status)
{
    if(v.size()==0) return;
    int j = 0;
    for (int i = 0; i < int(v.size()); i++)
        if (status[i])
            v[j++] = v[i];
    v.resize(j);
}


FeatureTracker::FeatureTracker()
{

}

void FeatureTracker::setMask()
{
    if(FISHEYE)
        mask = fisheye_mask.clone();
    else
        mask = cv::Mat(ROW, COL, CV_8UC1, cv::Scalar(255));

    // prefer to keep features that are tracked for long time
    vector<pair<int, pair<pair<cv::Point2f,cv::Point2f>, int>>> cnt_pts_id;

    for (unsigned int i = 0; i < forw_pts_cam0.size(); i++)
        cnt_pts_id.push_back(make_pair(track_cnt[i], 
        make_pair(make_pair(forw_pts_cam0[i],forw_pts_cam1[i]), ids[i])));

    // sort cnt_pts_id according to the traking times, points that frequently tracted 
    // are at the front of the list.
    sort(cnt_pts_id.begin(), cnt_pts_id.end(), [](
        const pair<int, pair<pair<cv::Point2f,cv::Point2f>, int>> &a, 
        const pair<int, pair<pair<cv::Point2f,cv::Point2f>, int>> &b)
         {
            return a.first > b.first;
         });

    forw_pts_cam0.clear();
    forw_pts_cam1.clear();
    ids.clear();
    track_cnt.clear();
    
    // reorganize track_cnt, forw_pts_cam0, ids and 
    // set mask around the keypoints with minimum distance
    for (auto &it : cnt_pts_id)
    {
        if (mask.at<uchar>(it.second.first.first) == 255)
        {
            forw_pts_cam0.push_back(it.second.first.first);
            forw_pts_cam1.push_back(it.second.first.second);
            ids.push_back(it.second.second);
            track_cnt.push_back(it.first);
            cv::circle(mask, it.second.first.first, MIN_DIST, 0, -1);
        }
    }
}

void FeatureTracker::addPoints()
{
    for (auto &p : new_pts_cam0)
    {
        forw_pts_cam0.push_back(p);
        ids.push_back(-1);
        track_cnt.push_back(1);
    }

    for (auto &p : new_pts_cam1)
    {
        forw_pts_cam1.push_back(p);
    }
}

void FeatureTracker::predictTracking(const cv::Matx33f &c_R_p)
{
    if(cur_pts_cam0.empty())
        return;

    // cout << "c_R_p: " << c_R_p << endl;

    vector<cv::Point2f> un_curr_pts;
    undistortPoints(cur_pts_cam0, model_cam0.K, model_cam0.model_name,
                    model_cam0.D, un_curr_pts, c_R_p);

    forw_pts_cam0 = distortPoints(un_curr_pts, model_cam0.K,
                             model_cam0.model_name, model_cam0.D);

    vector<unsigned char> status;
    for (int i = 0; i < int(forw_pts_cam0.size()); i++)
        if (!inBorder(forw_pts_cam0[i]))
            status.push_back(0);
        else
            status.push_back(1);

    reduceVector(forw_pts_cam0, status);
    reduceVector(forw_pts_cam1, status);
    reduceVector(cur_pts_cam0, status);
    reduceVector(cur_pts_cam1, status);
    reduceVector(ids, status);
    reduceVector(cur_un_pts_cam0, status);
    reduceVector(track_cnt, status);
}

void FeatureTracker::readImage(const cv::Mat &_img0, const cv::Mat &_img1,
                               double _cur_time, const cv::Matx33f &c_R_p)
{
    cv::Mat img0;
    cv::Mat img1;
    TicToc t_r;
    cur_time = _cur_time;

    // // equalize raw images from cam0 and cam1
    // if (EQUALIZE)
    // {
    //     cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE(3.0, cv::Size(5, 5));
    //     TicToc t_c;
    //     clahe->apply(_img0, img0);
    //     clahe->apply(_img1, img1);
    //     ROS_DEBUG("CLAHE costs: %fms", t_c.toc());
    // }
    // else
    // {
        img0 = _img0;
        img1 = _img1;
    // }
    // first frame
    // set parameters
    if (forw_img0.empty())
    {
        IMU_T_CAM0(cv::Range(0, 3), cv::Range(0, 3)).copyTo(imu_R_cam0);
        IMU_T_CAM1(cv::Range(0, 3), cv::Range(0, 3)).copyTo(imu_R_cam1);
        IMU_T_CAM0(cv::Range(0, 3), cv::Range(3, 4)).copyTo(imu_t_cam0);
        IMU_T_CAM1(cv::Range(0, 3), cv::Range(3, 4)).copyTo(imu_t_cam1);
        cur_img0 = forw_img0 = img0;
    }
    else
    {
        forw_img0 = img0;
    }
    // first frame
    if (forw_img1.empty())
    {
        cur_img1 = forw_img1 = img1;
    }
    else
    {
        forw_img1 = img1;
    }

    forw_pts_cam0.clear();
    forw_pts_cam1.clear();

    vector<unsigned char> status;
    
    // predict keypoints in new frame with optical flow,
    // and remove points that out of border
    if (cur_pts_cam0.size() > 0)
    {
        TicToc t_o;
        // forw_pts_cam0 = cur_pts_cam0;
        // cout << "cur_pts_cam0.size() (before) = " << cur_pts_cam0.size() <<endl;
        // cout << "forw_pts_cam0.size() (before) = " << forw_pts_cam0.size() <<endl;
        if(cur_pts_cam0.size() > 10)
        {
            // forw_pts_cam0 = cur_pts_cam0;
            // showMatchImg(cur_pts_cam0,cur_img0,forw_pts_cam0,forw_img0,std::vector<uchar>(0), "no_predict");
            TicToc t_pre;
            predictTracking(c_R_p);
            ROS_DEBUG("imu prediction costs: %fms", t_pre.toc());
            // forw_pts_cam0.clear();
            // showMatchImg(cur_pts_cam0,cur_img0,forw_pts_cam0,forw_img0,std::vector<uchar>(0), "predict");
        }
        else
        {
            forw_pts_cam0 = cur_pts_cam0;
        }
        
        // cout << "cur_pts_cam0.size() (after) = " << cur_pts_cam0.size() <<endl;
        // cout << "forw_pts_cam0.size() (after) = " << forw_pts_cam0.size() <<endl;
        // cout << "forw_pts_cam0.size() = " << forw_pts_cam0.size() <<endl;
        vector<cv::Point2f> predicted_pts = forw_pts_cam0;
        cv::calcOpticalFlowPyrLK(cur_img0, forw_img0, cur_pts_cam0, 
        forw_pts_cam0, status, cv::noArray(), cv::Size(21, 21), 5,
        cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 20, 0.01),
                             cv::OPTFLOW_USE_INITIAL_FLOW);

        for (int i = 0; i < int(forw_pts_cam0.size()); i++)
        {
            if (status[i] && !inBorder(forw_pts_cam0[i]))
                status[i] = 0;

            if(cv::norm(predicted_pts[i] - forw_pts_cam0[i]) > 20)
                status[i] = 0;
            // cout << "cv::norm(predicted_pts[i] - forw_pts_cam0[i]) = " 
            // << cv::norm(predicted_pts[i] - forw_pts_cam0[i]) << endl;
        }
        reduceVector(forw_pts_cam0, status);
        reduceVector(forw_pts_cam1, status);
        reduceVector(cur_pts_cam0, status);
        reduceVector(cur_pts_cam1, status);
        reduceVector(ids, status);
        reduceVector(cur_un_pts_cam0, status);
        reduceVector(track_cnt, status);

        // showMatchImg(cur_pts_cam0,cur_img0,forw_pts_cam0,forw_img0,std::vector<uchar>(0), "klt1");

        // static int frame_cnt = 0;
        // frame_cnt++;
        // static double t_sum = 0;
        // t_sum += t_o.toc();
        // ROS_DEBUG("temporal optical flow costs: %fms", t_sum/frame_cnt);
        ROS_DEBUG("temporal optical flow costs: %fms", t_o.toc());

        // temporal outlier remove for cam0
        if(cur_pts_cam0.size() >= 8)
        {
            TicToc t_f;  
            ROS_DEBUG("FM ransac begins");
            std::vector<cv::Point2f> un_cur_pts, un_forw_pts;
            undistortPoints(cur_pts_cam0, model_cam0.K, model_cam0.model_name,
                            model_cam0.D, un_cur_pts);
            undistortPoints(forw_pts_cam0, model_cam0.K, model_cam0.model_name,
                            model_cam0.D, un_forw_pts);
            // for (unsigned int i = 0; i < un_cur_pts.size(); i++)
            // {
            //     un_cur_pts[i] = FOCAL_LENGTH * un_cur_pts[i] + cv::Point2f(COL / 2.0, ROW / 2.0);
            //     un_forw_pts[i] = FOCAL_LENGTH * un_forw_pts[i] + cv::Point2f(COL / 2.0, ROW / 2.0);
            // }
            status.clear();
            cv::findFundamentalMat(un_cur_pts, un_forw_pts, cv::FM_RANSAC, F_THRESHOLD/FOCAL_LENGTH, 0.999, status);
            int size_a = un_cur_pts.size();
            reduceVector(forw_pts_cam0, status);
            reduceVector(forw_pts_cam1, status);
            reduceVector(cur_pts_cam0, status);
            reduceVector(cur_pts_cam1, status);
            reduceVector(ids, status);
            reduceVector(cur_un_pts_cam0, status);
            reduceVector(track_cnt, status);
            ROS_DEBUG("CAM0 FM ransac: %d -> %lu: %f", size_a, forw_pts_cam0.size(), 1.0 * forw_pts_cam0.size() / size_a);
            ROS_DEBUG("CAM0 FM ransac costs: %fms", t_f.toc());
        }
        // showMatchImg(cur_pts_cam0,cur_img0,forw_pts_cam0,forw_img0,std::vector<uchar>(0), "ransac1");

        // stereo match
        if(forw_pts_cam0.size() > 0){
            TicToc t_sm;
            status.clear();
            int size_a = forw_pts_cam0.size();
            stereoMatch(forw_pts_cam0, forw_img0, forw_pts_cam1, forw_img1, status);
            reduceVector(forw_pts_cam0, status);
            reduceVector(forw_pts_cam1, status);
            reduceVector(cur_pts_cam0, status);
            reduceVector(cur_pts_cam1, status);
            reduceVector(ids, status);
            reduceVector(cur_un_pts_cam0, status);
            reduceVector(track_cnt, status);
            ROS_DEBUG("Stereo match: %d -> %lu: %f", size_a, forw_pts_cam0.size(), 1.0 * forw_pts_cam0.size() / size_a);
            ROS_DEBUG("Stereo match costs: %fms", t_sm.toc());
        }

        // temporal outlier remove for cam1
        if(cur_pts_cam1.size() >= 8)
        {
            TicToc t_f;
            ROS_DEBUG("FM ransac begins");
            std::vector<cv::Point2f> un_cur_pts, un_forw_pts;
            undistortPoints(cur_pts_cam1, model_cam1.K, model_cam1.model_name,
                            model_cam1.D, un_cur_pts);
            undistortPoints(forw_pts_cam1, model_cam1.K, model_cam1.model_name,
                            model_cam1.D, un_forw_pts);
            cv::findFundamentalMat(un_cur_pts, un_forw_pts, cv::FM_RANSAC, F_THRESHOLD/FOCAL_LENGTH, 0.999, status);
            int size_a = un_cur_pts.size();
            reduceVector(forw_pts_cam0, status);
            reduceVector(forw_pts_cam1, status);
            reduceVector(cur_pts_cam0, status);
            reduceVector(cur_pts_cam1, status);
            reduceVector(ids, status);
            reduceVector(cur_un_pts_cam0, status);
            reduceVector(track_cnt, status);
            ROS_DEBUG("CAM1 FM ransac: %d -> %lu: %f", size_a, forw_pts_cam1.size(), 1.0 * forw_pts_cam1.size() / size_a);
            ROS_DEBUG("CAM1 FM ransac costs: %fms", t_f.toc());
        }

        // showMatchImg(forw_pts_cam0,forw_img0,forw_pts_cam1,forw_img1,std::vector<uchar>(0));
        for (auto &n : track_cnt)
            n++;
    }

    // static long int frame_cnt = 0;
    // static long int tracked_sum = 0;
    // frame_cnt++;
    // tracked_sum += forw_pts_cam0.size();
    // cout << "avg. Tracked points = " << tracked_sum/frame_cnt <<endl;
    // cout << "curr. Tracked points = " << forw_pts_cam0.size() <<endl;

    if (PUB_THIS_FRAME || (MAX_CNT/3.0 > (int)forw_pts_cam0.size()))
    // if (PUB_THIS_FRAME)
    // if( MAX_CNT/3.0 > (int)forw_pts_cam0.size())
    {
        ROS_DEBUG("set mask begins");
        TicToc t_m;
        setMask();
        // cv::imshow("mask", mask);
        // cv::waitKey(0);
        ROS_DEBUG("set mask costs %fms", t_m.toc());

        ROS_DEBUG("detect feature begins");
        TicToc t_t;
        int n_new_cnt = MAX_CNT - static_cast<int>(forw_pts_cam0.size());
        if (n_new_cnt > 0)
        {
            if(mask.empty())
                cout << "mask is empty " << endl;
            if (mask.type() != CV_8UC1)
                cout << "mask type wrong " << endl;
            if (mask.size() != forw_img0.size())
                cout << "wrong size " << endl;
            cv::goodFeaturesToTrack(forw_img0, new_pts_cam0, n_new_cnt, 0.01, MIN_DIST, mask);
            ROS_DEBUG("detect %d new features.", (int)new_pts_cam0.size());
            // stereo match
            vector<unsigned char> status(0);
            if(new_pts_cam0.size() > 0){
                stereoMatch(new_pts_cam0, forw_img0, new_pts_cam1, forw_img1, status);
                reduceVector(new_pts_cam0, status);
                reduceVector(new_pts_cam1, status);
            }
        }
        else
        {
            new_pts_cam0.clear();
            new_pts_cam1.clear();
        }
        ROS_DEBUG("detect feature costs: %fms", t_t.toc());

        ROS_DEBUG("add feature begins");
        TicToc t_a;
        addPoints();
        ROS_DEBUG("selectFeature costs: %fms", t_a.toc());
    }

    prev_un_pts_cam0 = cur_un_pts_cam0;
    cur_img0 = forw_img0;
    cur_img1 = forw_img1;
    cur_pts_cam0 = forw_pts_cam0;
    cur_pts_cam1 = forw_pts_cam1;
    undistortedPoints();
    prev_time = cur_time;
}

void FeatureTracker::checkBorder(const std::vector<cv::Point2f> &_pts, std::vector<unsigned char> &_status)
{
    for (int i = 0; i < int(_pts.size()); i++)
        if (_status[i] && !inBorder(_pts[i]))
            _status[i] = 0; // points that out of border set 0
}

void FeatureTracker::stereoMatch(
    const std::vector<cv::Point2f> &_cam0_pts, const cv::Mat &_img0,
    std::vector<cv::Point2f> &_cam1_pts, const cv::Mat &_img1,
    std::vector<unsigned char> &_status)
{
    if(_cam0_pts.size() == 0) return;

    ROS_DEBUG("stereo match input points size: %d",static_cast<int>(_cam0_pts.size()));

    std::vector<cv::Point2f> cam0_points_undistorted(0);
    std::vector<cv::Point2f> cam1_points_undistorted(0);

    // keypoint in cam0 cam1 image plane
    vector<cv::Point2f> cam0_pts = _cam0_pts;
    vector<cv::Point2f> cam1_pts;

    // inlier marker, outlier are set to status = 0
    std::vector<unsigned char> status;
    
    // cv::Mat(range(row_start, row_end+1), range(col_start, col_end+1))
    // start from 0
    // rotation matrix, project from cam0 to cam1
    cv::Matx33d cam1_R_cam0 = imu_R_cam1.t() * imu_R_cam0;
    // translation vector, w.r.t. cam1
    const cv::Vec3d cam1_t_cam0 = imu_R_cam1.t() * (imu_t_cam0 - imu_t_cam1);
    // std::cout << "imu_t_cam0=\n" << imu_t_cam0 << "\n";
    // std::cout << "imu_t_cam1=\n" << imu_t_cam1 << "\n";
    // std::cout << "cam1_t_cam0=\n" << cam1_t_cam0 << "\n";
    // std::cout << "imu_R_cam0=\n" << imu_R_cam0 << "\n";
    // std::cout << "imu_R_cam1=\n" << imu_R_cam1 << "\n";

    // Step 1. Projecting cam0 point to cam1 using rotation matrix
    // from stereo extrinsic
    // rotation matrix project from cam0 to cam1
    TicToc t_p;
    // undistort points in cam0 and rotate to cam1
    undistortPoints(cam0_pts, model_cam0.K, model_cam0.model_name, 
              model_cam0.D, cam0_points_undistorted, cam1_R_cam0);
    cam1_pts = distortPoints(cam0_points_undistorted, model_cam1.K,
                             model_cam1.model_name, model_cam1.D);

    float stereo_offset_pixel = -10;
    for(auto &pts:cam1_pts)
    {
        pts.x += stereo_offset_pixel;
    }

    // showMatchImg(cam0_pts,forw_img0,cam1_pts,forw_img1,status);

    ROS_DEBUG("stereo match predict cam1 point cost: %fms", t_p.toc());

    // Step 2. LK optical flow
    TicToc t_o;
    cv::calcOpticalFlowPyrLK(_img0, _img1, cam0_pts, cam1_pts,
                             status, cv::noArray(), cv::Size(21, 21), 5,
                             cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 20, 0.01),
                             cv::OPTFLOW_USE_INITIAL_FLOW);

    // remove points that out of border
    for (int i = 0; i < int(cam1_pts.size()); i++)
        if (status[i] && !inBorder(cam1_pts[i]))
            status[i] = 0;
    ROS_DEBUG("stereo match optical flow: %fms", t_o.toc());

    // showMatchImg(cam0_pts,forw_img0,cam1_pts,forw_img1,status,"before Essential");

    // Step 3. outlier removal base on essential matrix
    TicToc t_e;
    
    cam0_points_undistorted.clear();
    cam1_points_undistorted.clear();
    // undistort points
    undistortPoints(cam0_pts, model_cam0.K, model_cam0.model_name, 
              model_cam0.D, cam0_points_undistorted);
    undistortPoints(cam1_pts, model_cam1.K, model_cam1.model_name, 
             model_cam1.D, cam1_points_undistorted);

    if (cam0_points_undistorted.size() > 8 || cam1_points_undistorted.size() > 8)
        cv::findFundamentalMat(cam0_points_undistorted, cam1_points_undistorted,
                               cv::FM_RANSAC, F_THRESHOLD / FOCAL_LENGTH, 0.999, status);
    else{
        // Compute the essential matrix.
        const cv::Matx33d cam1_t_cam0_hat(
            0.0, -cam1_t_cam0[2], cam1_t_cam0[1],
            cam1_t_cam0[2], 0.0, -cam1_t_cam0[0],
            -cam1_t_cam0[1], cam1_t_cam0[0], 0.0);
        const cv::Matx33d E = cam1_t_cam0_hat * cam1_R_cam0;

        // cout << "cam1_R_cam0 = " << cam1_R_cam0 << endl;

        // std::cout << "cam1_t_cam0_hat=\n" << cam1_t_cam0_hat << "\n";
        // std::cout << "E=\n" << E << "\n";

        for (unsigned int i = 0; i < cam0_points_undistorted.size(); ++i)
        {
            if (status[i] == 0)
                continue;
            cv::Vec3d pt0(cam0_points_undistorted[i].x,
                        cam0_points_undistorted[i].y, 1.0);
            cv::Vec3d pt1(cam1_points_undistorted[i].x,
                        cam1_points_undistorted[i].y, 1.0);
            cv::Vec3d epipolar_line = E * pt0;
            // cout << "epipolar line: " <<  epipolar_line << endl;
            double error = fabs((pt1.t() * epipolar_line)[0]) / sqrt(
                                                                    epipolar_line[0] * epipolar_line[0] +
                                                                    epipolar_line[1] * epipolar_line[1]);
            if (error * FOCAL_LENGTH > STEREO_THRESHOLD)
                status[i] = 0;

            if((pt0[0] - pt1[0]) < 0)
                status[i] = 0;
        }
    }

    ROS_DEBUG("stereo match essential matrix removal cost: %fms", t_e.toc());

    // showUndistortionCam0("undistort cam0");
    // showUndistortionCam1("undistort cam1");
    // showMatchImg(cam0_pts,forw_img0,cam1_pts,forw_img1,status);
    // showMatchImg(cam0_pts,forw_img0,cam1_pts,forw_img1,status,"after Essential");
    _cam1_pts = cam1_pts;
    _status = status;
}


bool FeatureTracker::updateID(unsigned int i)
{
    if (i < ids.size())
    {
        if (ids[i] == -1) // new feature, give id
            ids[i] = n_id++;
        return true;
    }
    else
        return false;
}

void FeatureTracker::readIntrinsicParameter(const string &calib_file, 
                                                camodocal::CameraPtr &camera)
{
    ROS_INFO("reading paramerter of camera %s", calib_file.c_str());
    camera = CameraFactory::instance()->generateCameraFromYamlFile(calib_file);
}


void FeatureTracker::showUndistortionCam0(const string &name)
{
    cv::Mat undistortedImg(ROW + 300, COL + 1200, CV_8UC1, cv::Scalar(0));
    vector<cv::Point2f> cv_distortedp, cv_undistortedp;
    vector<Eigen::Vector2d> distortedp, undistortedp;

    for (int i = 0; i < COL; i++)
        for (int j = 0; j < ROW; j++)
            cv_distortedp.emplace_back(i,j);

    undistortPoints(cv_distortedp, model_cam0.K, model_cam0.model_name, 
                    model_cam0.D, cv_undistortedp);

    
    for(int i =0; i < (int)cv_distortedp.size(); i++)
    {
        distortedp.emplace_back(cv_distortedp[i].x, cv_distortedp[i].y);
        undistortedp.emplace_back(cv_undistortedp[i].x, cv_undistortedp[i].y);
    }

    // for (int i = 0; i < COL; i++)
    //     for (int j = 0; j < ROW; j++)
    //     {
    //         Eigen::Vector2d a(i, j);
    //         Eigen::Vector3d b;
    //         m_camera_0->liftProjective(a, b);
    //         distortedp.push_back(a);
    //         undistortedp.push_back(Eigen::Vector2d(b.x() / b.z(), b.y() / b.z()));
    //         //printf("%f,%f->%f,%f,%f\n)\n", a.x(), a.y(), b.x(), b.y(), b.z());
    //     }

    
    for (int i = 0; i < int(undistortedp.size()); i++)
    {
        cv::Mat pp(3, 1, CV_32FC1);
        pp.at<float>(0, 0) = undistortedp[i].x() * FOCAL_LENGTH + undistortedImg.cols / 2;
        pp.at<float>(1, 0) = undistortedp[i].y() * FOCAL_LENGTH + undistortedImg.rows / 2;
        pp.at<float>(2, 0) = 1.0;
        //cout << trackerData[0].K << endl;
        //printf("%lf %lf\n", p.at<float>(1, 0), p.at<float>(0, 0));
        //printf("%lf %lf\n", pp.at<float>(1, 0), pp.at<float>(0, 0));
        if (pp.at<float>(1, 0) >= 0 && pp.at<float>(1, 0) < undistortedImg.rows 
        && pp.at<float>(0, 0) >= 0 && pp.at<float>(0, 0) < undistortedImg.cols)
        {
            undistortedImg.at<uchar>(pp.at<float>(1, 0), pp.at<float>(0, 0)) = cur_img0.at<uchar>(distortedp[i].y(), distortedp[i].x());
        }
        else
        {
            //ROS_ERROR("(%f %f) -> (%f %f)", distortedp[i].y, distortedp[i].x, pp.at<float>(1, 0), pp.at<float>(0, 0));
        }
    }
    cv::imshow(name, undistortedImg);
    cv::waitKey(0);
}

void FeatureTracker::showUndistortionCam1(const string &name)
{
    cv::Mat undistortedImg(ROW + 300, COL + 1200, CV_8UC1, cv::Scalar(0));
    vector<cv::Point2f> cv_distortedp, cv_undistortedp;
    vector<Eigen::Vector2d> distortedp, undistortedp;

    for (int i = 0; i < COL; i++)
        for (int j = 0; j < ROW; j++)
            cv_distortedp.emplace_back(i,j);

    undistortPoints(cv_distortedp, model_cam1.K, model_cam1.model_name, 
                    model_cam1.D, cv_undistortedp);
    
    for(int i =0; i < (int)cv_distortedp.size(); i++)
    {
        distortedp.emplace_back(cv_distortedp[i].x, cv_distortedp[i].y);
        undistortedp.emplace_back(cv_undistortedp[i].x, cv_undistortedp[i].y);
    }
    
    for (int i = 0; i < int(undistortedp.size()); i++)
    {
        cv::Mat pp(3, 1, CV_32FC1);
        pp.at<float>(0, 0) = undistortedp[i].x() * FOCAL_LENGTH + undistortedImg.cols / 2;
        pp.at<float>(1, 0) = undistortedp[i].y() * FOCAL_LENGTH + undistortedImg.rows / 2;
        pp.at<float>(2, 0) = 1.0;
        //cout << trackerData[0].K << endl;
        //printf("%lf %lf\n", p.at<float>(1, 0), p.at<float>(0, 0));
        //printf("%lf %lf\n", pp.at<float>(1, 0), pp.at<float>(0, 0));
        if (pp.at<float>(1, 0) >= 0 && pp.at<float>(1, 0) < undistortedImg.rows 
        && pp.at<float>(0, 0) >= 0 && pp.at<float>(0, 0) < undistortedImg.cols)
        {
            undistortedImg.at<uchar>(pp.at<float>(1, 0), pp.at<float>(0, 0)) = cur_img1.at<uchar>(distortedp[i].y(), distortedp[i].x());
        }
        else
        {
            //ROS_ERROR("(%f %f) -> (%f %f)", distortedp[i].y, distortedp[i].x, pp.at<float>(1, 0), pp.at<float>(0, 0));
        }
    }
    cv::imshow(name, undistortedImg);
    cv::waitKey(0);
}

void FeatureTracker::undistortedPoints()
{
    cur_un_pts_cam0.clear();
    cur_un_pts_cam1.clear();

    cur_un_pts_map.clear();
    //cv::undistortPoints(cur_pts_cam0, un_pts, K, cv::Mat());
    // undistort points
    undistortPoints(cur_pts_cam0, model_cam0.K, model_cam0.model_name, 
              model_cam0.D, cur_un_pts_cam0);
    undistortPoints(cur_pts_cam1, model_cam1.K, model_cam1.model_name, 
              model_cam1.D, cur_un_pts_cam1);

    for (unsigned int i = 0; i < cur_un_pts_cam0.size(); i++)
      cur_un_pts_map.insert(make_pair(ids[i], cur_un_pts_cam0[i]));

    // for (unsigned int i = 0; i < cur_pts_cam0.size(); i++)
    // {
    //     Eigen::Eigen::Vector2d a(cur_pts_cam0[i].x, cur_pts_cam0[i].y);
    //     Eigen::Vector3d b;
    //     m_camera_0->liftProjective(a, b);
    //     cur_un_pts_cam0.push_back(cv::Point2f(b.x() / b.z(), b.y() / b.z()));
    //     cur_un_pts_map.insert(make_pair(ids[i], cv::Point2f(b.x() / b.z(), b.y() / b.z())));
    //     //printf("cur pts id %d %f %f", ids[i], cur_un_pts_cam0[i].x, cur_un_pts_cam0[i].y);
    // }

    // caculate points velocity
    if (!prev_un_pts_map.empty())
    {
        double dt = cur_time - prev_time;
        pts_velocity_cam0.clear();
        for (unsigned int i = 0; i < cur_un_pts_cam0.size(); i++)
        {
            if (ids[i] != -1)
            {
                std::map<int, cv::Point2f>::iterator it;
                it = prev_un_pts_map.find(ids[i]);
                if (it != prev_un_pts_map.end())
                {
                    double v_x = (cur_un_pts_cam0[i].x - it->second.x) / dt;
                    double v_y = (cur_un_pts_cam0[i].y - it->second.y) / dt;
                    pts_velocity_cam0.push_back(cv::Point2f(v_x, v_y));
                }
                else
                    pts_velocity_cam0.push_back(cv::Point2f(0, 0));
            }
            else
            {
                pts_velocity_cam0.push_back(cv::Point2f(0, 0));
            }
        }
    }
    else
    {
        for (unsigned int i = 0; i < cur_pts_cam0.size(); i++)
        {
            pts_velocity_cam0.push_back(cv::Point2f(0, 0));
        }
    }
    prev_un_pts_map = cur_un_pts_map;
}

void FeatureTracker::showMatchImg(const std::vector<cv::Point2f> &p1, const cv::Mat &img1,
                                  const std::vector<cv::Point2f> &p2, const cv::Mat &img2,
                                  const std::vector<unsigned char> inlier_marker, 
                                  const std::string win_name)
{
  vector<cv::DMatch> match(0);
  vector<cv::KeyPoint> matched_cam0_points;
  vector<cv::KeyPoint> matched_cam1_points;
  int match_num = 0;
  for (unsigned int i = 0; i < p1.size(); i++)
  {
    if(inlier_marker.size()>0)
      if(inlier_marker[i] == 0) continue;
    matched_cam0_points.push_back(cv::KeyPoint(p1[i], 0.1));
    matched_cam1_points.push_back(cv::KeyPoint(p2[i], 0.1));
    match.push_back(cv::DMatch(match_num, match_num, 1));
    ++match_num;
  }
  cv::Mat img_matches;
  cv::drawMatches(img1, matched_cam0_points, img2, matched_cam1_points, match, img_matches);
  cv::imshow(win_name, img_matches);
  cv::waitKey(0);
}


void FeatureTracker::undistortPoints(
    const vector<cv::Point2f>& pts_in,
    const cv::Vec4d& intrinsics,
    const string& distortion_model,
    const cv::Vec4d& distortion_coeffs,
    vector<cv::Point2f>& pts_out,
    const cv::Matx33d &rectification_matrix,
    const cv::Vec4d &new_intrinsics) {

  if (pts_in.size() == 0) return;

  const cv::Matx33d K(
      intrinsics[0], 0.0, intrinsics[2],
      0.0, intrinsics[1], intrinsics[3],
      0.0, 0.0, 1.0);

  const cv::Matx33d K_new(
      new_intrinsics[0], 0.0, new_intrinsics[2],
      0.0, new_intrinsics[1], new_intrinsics[3],
      0.0, 0.0, 1.0);

  if (distortion_model == "radtan") {
    cv::undistortPoints(pts_in, pts_out, K, distortion_coeffs,
                        rectification_matrix, K_new);
  } else if (distortion_model == "equidistant") {
    cv::fisheye::undistortPoints(pts_in, pts_out, K, distortion_coeffs,
                                 rectification_matrix, K_new);
  } else {
    ROS_WARN_ONCE("The model %s is unrecognized, use radtan instead...",
                  distortion_model.c_str());
    cv::undistortPoints(pts_in, pts_out, K, distortion_coeffs,
                        rectification_matrix, K_new);
  }

  return;
}


vector<cv::Point2f> FeatureTracker::distortPoints(
    const vector<cv::Point2f>& pts_in,
    const cv::Vec4d& intrinsics,
    const string& distortion_model,
    const cv::Vec4d& distortion_coeffs) 
{

  const cv::Matx33d K(intrinsics[0], 0.0, intrinsics[2],
                      0.0, intrinsics[1], intrinsics[3],
                      0.0, 0.0, 1.0);

  vector<cv::Point2f> pts_out;
  if (distortion_model == "radtan") {
    vector<cv::Point3f> homogenous_pts;
    cv::convertPointsToHomogeneous(pts_in, homogenous_pts);
    cv::projectPoints(homogenous_pts, cv::Vec3d::zeros(), cv::Vec3d::zeros(), K,
                      distortion_coeffs, pts_out);
  } else if (distortion_model == "equidistant") {
    cv::fisheye::distortPoints(pts_in, pts_out, K, distortion_coeffs);
  } else {
    ROS_WARN_ONCE("The model %s is unrecognized, using radtan instead...",
                  distortion_model.c_str());
    vector<cv::Point3f> homogenous_pts;
    cv::convertPointsToHomogeneous(pts_in, homogenous_pts);
    cv::projectPoints(homogenous_pts, cv::Vec3d::zeros(), cv::Vec3d::zeros(), K,
                      distortion_coeffs, pts_out);
  }

  return pts_out;
}

cv::Point2f FeatureTracker::distortPoint(
    const cv::Point2f& pt_in,
    const cv::Vec4d& intrinsics,
    const string& distortion_model,
    const cv::Vec4d& distortion_coeffs) 
{

  vector<cv::Point2f> pts_in, pts_out;
  pts_in.push_back(pt_in);

  const cv::Matx33d K(intrinsics[0], 0.0, intrinsics[2],
                      0.0, intrinsics[1], intrinsics[3],
                      0.0, 0.0, 1.0);

  if (distortion_model == "radtan") {
    vector<cv::Point3f> homogenous_pts;
    cv::convertPointsToHomogeneous(pts_in, homogenous_pts);
    cv::projectPoints(homogenous_pts, cv::Vec3d::zeros(), cv::Vec3d::zeros(), K,
                      distortion_coeffs, pts_out);
  } else if (distortion_model == "equidistant") {
    cv::fisheye::distortPoints(pts_in, pts_out, K, distortion_coeffs);
  } else {
    ROS_WARN_ONCE("The model %s is unrecognized, using radtan instead...",
                  distortion_model.c_str());
    vector<cv::Point3f> homogenous_pts;
    cv::convertPointsToHomogeneous(pts_in, homogenous_pts);
    cv::projectPoints(homogenous_pts, cv::Vec3d::zeros(), cv::Vec3d::zeros(), K,
                      distortion_coeffs, pts_out);
  }

  cv::Point2f pt_out;

  pt_out = pts_out[0];

  return pt_out;
}

void FeatureTracker::triangulatePoint(const Eigen::Matrix<double, 3, 4> &Pose0, const Eigen::Matrix<double, 3, 4> &Pose1,
                                      const Vector2d &point0, const Vector2d &point1, Vector3d &point_3d)
{
    Matrix4d design_matrix = Matrix4d::Zero();
    design_matrix.row(0) = point0[0] * Pose0.row(2) - Pose0.row(0);
    design_matrix.row(1) = point0[1] * Pose0.row(2) - Pose0.row(1);
    design_matrix.row(2) = point1[0] * Pose1.row(2) - Pose1.row(0);
    design_matrix.row(3) = point1[1] * Pose1.row(2) - Pose1.row(1);
    Vector4d triangulated_point;
    triangulated_point =
        design_matrix.jacobiSvd(Eigen::ComputeFullV).matrixV().rightCols<1>();
    point_3d(0) = triangulated_point(0) / triangulated_point(3);
    point_3d(1) = triangulated_point(1) / triangulated_point(3);
    point_3d(2) = triangulated_point(2) / triangulated_point(3);
}

// void FeatureTracker::twoPointRansac(
//     const vector<cv::Point2f>& pts1, const vector<cv::Point2f>& pts2,
//     const cv::Matx33f& R_p_c, const cv::Vec4d& intrinsics,
//     const std::string& distortion_model,
//     const cv::Vec4d& distortion_coeffs,
//     const double& inlier_error,
//     const double& success_probability,
//     vector<int>& inlier_markers) 
// {

//   // Check the size of input point size.
//   if (pts1.size() != pts2.size())
//     ROS_ERROR("Sets of different size (%lu and %lu) are used...",
//         pts1.size(), pts2.size());

//   double norm_pixel_unit = 2.0 / (intrinsics[0]+intrinsics[1]);
//   int iter_num = static_cast<int>(
//       ceil(log(1-success_probability) / log(1-0.6*0.6)));

//   // Initially, mark all points as inliers.
//   inlier_markers.clear();
//   inlier_markers.resize(pts1.size(), 1);

//   // Undistort all the points.
//   vector<cv::Point2f> pts1_undistorted(pts1.size());
//   vector<cv::Point2f> pts2_undistorted(pts2.size());
//   undistortPoints(
//       pts1, intrinsics, distortion_model,
//       distortion_coeffs, pts1_undistorted);
//   undistortPoints(
//       pts2, intrinsics, distortion_model,
//       distortion_coeffs, pts2_undistorted);

//   // Compenstate the points in the previous image with
//   // the relative rotation.
//   for (auto& pt : pts1_undistorted) {
//     cv::Vec3f pt_h(pt.x, pt.y, 1.0f);
//     //Vec3f pt_hc = dR * pt_h;
//     cv::Vec3f pt_hc = R_p_c * pt_h;
//     pt.x = pt_hc[0];
//     pt.y = pt_hc[1];
//   }

//   // Normalize the points to gain numerical stability.
//   float scaling_factor = 0.0f;
//   rescalePoints(pts1_undistorted, pts2_undistorted, scaling_factor);
//   norm_pixel_unit *= scaling_factor;

//   // Compute the difference between previous and current points,
//   // which will be used frequently later.
//   vector<cv::Point2d> pts_diff(pts1_undistorted.size());
//   for (int i = 0; i < pts1_undistorted.size(); ++i)
//     pts_diff[i] = pts1_undistorted[i] - pts2_undistorted[i];

//   // Mark the point pairs with large difference directly.
//   // BTW, the mean distance of the rest of the point pairs
//   // are computed.
//   double mean_pt_distance = 0.0;
//   int raw_inlier_cntr = 0;
//   for (int i = 0; i < pts_diff.size(); ++i) {
//     double distance = sqrt(pts_diff[i].dot(pts_diff[i]));
//     // 25 pixel distance is a pretty large tolerance for normal motion.
//     // However, to be used with aggressive motion, this tolerance should
//     // be increased significantly to match the usage.
//     if (distance > 50.0*norm_pixel_unit) {
//       inlier_markers[i] = 0;
//     } else {
//       mean_pt_distance += distance;
//       ++raw_inlier_cntr;
//     }
//   }
//   mean_pt_distance /= raw_inlier_cntr;

//   // If the current number of inliers is less than 3, just mark
//   // all input as outliers. This case can happen with fast
//   // rotation where very few features are tracked.
//   if (raw_inlier_cntr < 3) {
//     for (auto& marker : inlier_markers) marker = 0;
//     return;
//   }

//   // Before doing 2-point RANSAC, we have to check if the motion
//   // is degenerated, meaning that there is no translation between
//   // the frames, in which case, the model of the RANSAC does not
//   // work. If so, the distance between the matched points will
//   // be almost 0.
//   //if (mean_pt_distance < inlier_error*norm_pixel_unit) {
//   if (mean_pt_distance < norm_pixel_unit) {
//     //ROS_WARN_THROTTLE(1.0, "Degenerated motion...");
//     for (int i = 0; i < pts_diff.size(); ++i) {
//       if (inlier_markers[i] == 0) continue;
//       if (sqrt(pts_diff[i].dot(pts_diff[i])) >
//           inlier_error*norm_pixel_unit)
//         inlier_markers[i] = 0;
//     }
//     return;
//   }

//   // In the case of general motion, the RANSAC model can be applied.
//   // The three column corresponds to tx, ty, and tz respectively.
//   Eigen::MatrixXd coeff_t(pts_diff.size(), 3);
//   for (int i = 0; i < pts_diff.size(); ++i) {
//     coeff_t(i, 0) = pts_diff[i].y;
//     coeff_t(i, 1) = -pts_diff[i].x;
//     coeff_t(i, 2) = pts1_undistorted[i].x*pts2_undistorted[i].y -
//       pts1_undistorted[i].y*pts2_undistorted[i].x;
//   }

//   vector<int> raw_inlier_idx;
//   for (int i = 0; i < inlier_markers.size(); ++i) {
//     if (inlier_markers[i] != 0)
//       raw_inlier_idx.push_back(i);
//   }

//   vector<int> best_inlier_set;
//   double best_error = 1e10;
//   random_numbers::RandomNumberGenerator random_gen;

//   for (int iter_idx = 0; iter_idx < iter_num; ++iter_idx) {
//     // Randomly select two point pairs.
//     // Although this is a weird way of selecting two pairs, but it
//     // is able to efficiently avoid selecting repetitive pairs.
//     int select_idx1 = random_gen.uniformInteger(
//         0, raw_inlier_idx.size()-1);
//     int select_idx_diff = random_gen.uniformInteger(
//         1, raw_inlier_idx.size()-1);
//     int select_idx2 = select_idx1+select_idx_diff<raw_inlier_idx.size() ?
//       select_idx1+select_idx_diff :
//       select_idx1+select_idx_diff-raw_inlier_idx.size();

//     int pair_idx1 = raw_inlier_idx[select_idx1];
//     int pair_idx2 = raw_inlier_idx[select_idx2];

//     // Construct the model;
//     Eigen::Vector2d coeff_tx(coeff_t(pair_idx1, 0), coeff_t(pair_idx2, 0));
//     Eigen::Vector2d coeff_ty(coeff_t(pair_idx1, 1), coeff_t(pair_idx2, 1));
//     Eigen::Vector2d coeff_tz(coeff_t(pair_idx1, 2), coeff_t(pair_idx2, 2));
//     vector<double> coeff_l1_norm(3);
//     coeff_l1_norm[0] = coeff_tx.lpNorm<1>();
//     coeff_l1_norm[1] = coeff_ty.lpNorm<1>();
//     coeff_l1_norm[2] = coeff_tz.lpNorm<1>();
//     int base_indicator = min_element(coeff_l1_norm.begin(),
//         coeff_l1_norm.end())-coeff_l1_norm.begin();

//     Vector3d model(0.0, 0.0, 0.0);
//     if (base_indicator == 0) {
//       Eigen::Matrix2d A;
//       A << coeff_ty, coeff_tz;
//       Eigen::Vector2d solution = A.inverse() * (-coeff_tx);
//       model(0) = 1.0;
//       model(1) = solution(0);
//       model(2) = solution(1);
//     } else if (base_indicator ==1) {
//       Eigen::Matrix2d A;
//       A << coeff_tx, coeff_tz;
//       Eigen::Vector2d solution = A.inverse() * (-coeff_ty);
//       model(0) = solution(0);
//       model(1) = 1.0;
//       model(2) = solution(1);
//     } else {
//       Eigen::Matrix2d A;
//       A << coeff_tx, coeff_ty;
//       Eigen::Vector2d solution = A.inverse() * (-coeff_tz);
//       model(0) = solution(0);
//       model(1) = solution(1);
//       model(2) = 1.0;
//     }

//     // Find all the inliers among point pairs.
//     Eigen::VectorXd error = coeff_t * model;

//     vector<int> inlier_set;
//     for (int i = 0; i < error.rows(); ++i) {
//       if (inlier_markers[i] == 0) continue;
//       if (std::abs(error(i)) < inlier_error*norm_pixel_unit)
//         inlier_set.push_back(i);
//     }

//     // If the number of inliers is small, the current
//     // model is probably wrong.
//     if (inlier_set.size() < 0.2*pts1_undistorted.size())
//       continue;

//     // Refit the model using all of the possible inliers.
//     Eigen::VectorXd coeff_tx_better(inlier_set.size());
//     Eigen::VectorXd coeff_ty_better(inlier_set.size());
//     Eigen::VectorXd coeff_tz_better(inlier_set.size());
//     for (int i = 0; i < inlier_set.size(); ++i) {
//       coeff_tx_better(i) = coeff_t(inlier_set[i], 0);
//       coeff_ty_better(i) = coeff_t(inlier_set[i], 1);
//       coeff_tz_better(i) = coeff_t(inlier_set[i], 2);
//     }

//     Vector3d model_better(0.0, 0.0, 0.0);
//     if (base_indicator == 0) {
//       Eigen::MatrixXd A(inlier_set.size(), 2);
//       A << coeff_ty_better, coeff_tz_better;
//       Eigen::Vector2d solution =
//           (A.transpose() * A).inverse() * A.transpose() * (-coeff_tx_better);
//       model_better(0) = 1.0;
//       model_better(1) = solution(0);
//       model_better(2) = solution(1);
//     } else if (base_indicator ==1) {
//       Eigen::MatrixXd A(inlier_set.size(), 2);
//       A << coeff_tx_better, coeff_tz_better;
//       Eigen::Vector2d solution =
//           (A.transpose() * A).inverse() * A.transpose() * (-coeff_ty_better);
//       model_better(0) = solution(0);
//       model_better(1) = 1.0;
//       model_better(2) = solution(1);
//     } else {
//       Eigen::MatrixXd A(inlier_set.size(), 2);
//       A << coeff_tx_better, coeff_ty_better;
//       Eigen::Vector2d solution =
//           (A.transpose() * A).inverse() * A.transpose() * (-coeff_tz_better);
//       model_better(0) = solution(0);
//       model_better(1) = solution(1);
//       model_better(2) = 1.0;
//     }

//     // Compute the error and upate the best model if possible.
//     Eigen::VectorXd new_error = coeff_t * model_better;

//     double this_error = 0.0;
//     for (const auto& inlier_idx : inlier_set)
//       this_error += std::abs(new_error(inlier_idx));
//     this_error /= inlier_set.size();

//     if (inlier_set.size() > best_inlier_set.size()) {
//       best_error = this_error;
//       best_inlier_set = inlier_set;
//     }
//   }

//   // Fill in the markers.
//   inlier_markers.clear();
//   inlier_markers.resize(pts1.size(), 0);
//   for (const auto& inlier_idx : best_inlier_set)
//     inlier_markers[inlier_idx] = 1;

//   //printf("inlier ratio: %lu/%lu\n",
//   //    best_inlier_set.size(), inlier_markers.size());

//   return;
// }

// void FeatureTracker::rescalePoints(
//     vector<cv::Point2f>& pts1, vector<cv::Point2f>& pts2,
//     float& scaling_factor) 
// {

//   scaling_factor = 0.0f;

//   for (int i = 0; i < pts1.size(); ++i) {
//     scaling_factor += sqrt(pts1[i].dot(pts1[i]));
//     scaling_factor += sqrt(pts2[i].dot(pts2[i]));
//   }

//   scaling_factor = (pts1.size()+pts2.size()) /
//     scaling_factor * sqrt(2.0f);

//   for (int i = 0; i < pts1.size(); ++i) {
//     pts1[i] *= scaling_factor;
//     pts2[i] *= scaling_factor;
//   }

//   return;
// }